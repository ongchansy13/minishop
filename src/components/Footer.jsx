import { Link } from "react-router-dom";

import { motion } from "framer-motion";

const Footer = () => {
  return (
    <footer className="bg-black text-white py-12 relative ">
      <div className="max-w-[1200px] mx-auto grid md:grid-cols-4 gap-8 px-5 md:place-items-center">
        <div className="flex flex-col gap-5 cursor-pointer">
          <p className="font-bold">MINISHOP</p>
          <p>
            Far far away, behind the word mountains, far from the countries
            Vokalia and Consonantia.
          </p>

          <div className="flex items-center gap-4">
            <motion.div
              initial="hidden"
              whileInView="visible"
              viewport={{ once: true, amount: 0.8 }}
              transition={{ duration: 0.5 }}
              variants={{
                hidden: { opacity: 0, y: 50 },
                visible: { opacity: 1, y: 0 },
              }}
              className="bg-yellow-600 pt-2 px-2 pb-1 rounded-full"
            >
              <ion-icon
                style={{ fontSize: "30px" }}
                name="logo-facebook"
              ></ion-icon>
            </motion.div>

            <motion.div
              initial="hidden"
              whileInView="visible"
              viewport={{ once: true, amount: 0.8 }}
              transition={{ duration: 0.5 }}
              variants={{
                hidden: { opacity: 0, y: 50 },
                visible: { opacity: 1, y: 0 },
              }}
              className="bg-yellow-600 pt-2 px-2 pb-1 rounded-full"
            >
              <ion-icon
                style={{ fontSize: "30px" }}
                name="logo-twitter"
              ></ion-icon>
            </motion.div>

            <motion.div
              initial="hidden"
              whileInView="visible"
              viewport={{ once: true, amount: 0.8 }}
              transition={{ duration: 0.5 }}
              variants={{
                hidden: { opacity: 0, y: 50 },
                visible: { opacity: 1, y: 0 },
              }}
              className="bg-yellow-600 pt-2 px-2 pb-1 rounded-full"
            >
              <ion-icon
                style={{ fontSize: "30px" }}
                name="logo-instagram"
              ></ion-icon>
            </motion.div>
          </div>
        </div>

        <div className="flex flex-col gap-5 cursor-pointer">
          <p className="font-bold">MENU</p>
          <ul className="flex flex-col gap-4">
            <li>Shop</li>
            <li>About</li>
            <li>Journal</li>
            <li>Contact Us</li>
          </ul>
        </div>
        <div className="flex flex-col gap-5 cursor-pointer">
          <p className="font-bold">HELP</p>
          <ul className="flex flex-col gap-4">
            <li>Shipping Information</li>
            <li>Returns & Exchange</li>
            <li>Terms & Conditions</li>
            <li>Privacy Policy FAQs</li>
          </ul>
        </div>
        <div className="flex flex-col gap-5 cursor-pointer ">
          <p className="font-bold">HAVE A QUESTIONS?</p>
          <ul className="flex flex-col gap-4">
            <li className="flex gap-4 items-center">
              <ion-icon
                style={{ fontSize: "35px" }}
                name="location-sharp"
              ></ion-icon>
              <p> 203 Fake St. Mountain View, San Francisco, California, USA</p>
            </li>
            <li className="flex gap-4 items-center">
              <ion-icon
                style={{ fontSize: "25px" }}
                name="call-sharp"
              ></ion-icon>
              <p> +2 392 3929 210</p>
            </li>
            <li className="flex gap-4 items-center">
              <ion-icon style={{ fontSize: "25px" }} name="mail"></ion-icon>
              <p> info@yourdomain.com</p>
            </li>
          </ul>
        </div>
      </div>

      <div className="py-20 px-5">
        <p className="text-center">
          Copyright ©2024 All rights reserved | This template is made with ❤️ by
          Colorlib
        </p>
      </div>

      <div className="absolute -top-5 left-[50%] px-4 py-3 bg-white rounded-full">
        <Link to="/">
          <ion-icon style={{ color: "black" }} name="chevron-up"></ion-icon>
        </Link>
      </div>
    </footer>
  );
};

export default Footer;
