import CartTotal from "./CartTotal";
import Payment from "./Payment";

const PaymentMethods = () => {
  return (
    <>
      <section className="py-12 px-5">
        <div className="max-w-[1000px] mx-auto grid gap-2  md:gap-10 md:grid-cols-2">
          <CartTotal />
          <Payment />
        </div>
      </section>
    </>
  );
};

export default PaymentMethods;
