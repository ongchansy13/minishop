import { useState, useEffect } from "react";

const Countdown = ({ targetDate }) => {
  const calculateTimeLeft = () => {
    const now = new Date().getTime();
    const difference = targetDate - now;

    if (difference <= 0) {
      return {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
      };
    }

    const days = Math.floor(difference / (1000 * 60 * 60 * 24));
    const hours = Math.floor(
      (difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    const minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((difference % (1000 * 60)) / 1000);

    return {
      days,
      hours,
      minutes,
      seconds,
    };
  };

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

  useEffect(() => {
    const timer = setInterval(() => {
      setTimeLeft(calculateTimeLeft());
    }, 1000);

    return () => clearInterval(timer);
  });

  return (
    <div className="countdown flex gap-12 py-10">
      <div className="flex flex-col items-center gap-2">
        <span className="font-bold text-3xl">{timeLeft.days}</span>
        <span className="font-medium text-lg">Days</span>
      </div>
      <div className="flex flex-col items-center gap-3">
        <span className="font-bold text-3xl">{timeLeft.hours}</span>
        <span className="font-medium text-lg">Hours</span>
      </div>
      <div className="flex flex-col items-center gap-3">
        <span className="font-bold text-3xl">{timeLeft.minutes}</span>
        <span className="font-medium text-lg">Minutes</span>
      </div>
      <div className="flex flex-col items-center gap-3">
        <span className="font-bold text-3xl">{timeLeft.seconds}</span>
        <span className="font-medium text-lg">Seconds</span>
      </div>
    </div>
  );
};

export default Countdown;
