import { motion } from "framer-motion";

import topbanner from "../assets/bg_6.jpg";

const TopBanner = ({ name, subname, about }) => {
  return (
    <section className="z-0 mx-auto">
      <div className="w-full  relative">
        <img
          className="w-full h-[500px] object-cover object-center"
          src={topbanner}
          alt=""
        />
        <div className=" flex flex-col items-center justify-center gap-5 absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2">
          <motion.h6
            initial="hidden"
            whileInView="visible"
            viewport={{ once: true, amount: 0.8 }}
            transition={{ duration: 0.5 }}
            variants={{
              hidden: { opacity: 0, y: 50 },
              visible: { opacity: 1, y: 0 },
            }}
            className="font-bold text-gray-500"
          >
            {name} <span className="ml-5">{subname}</span>
          </motion.h6>
          <motion.h1
            initial="hidden"
            whileInView="visible"
            viewport={{ once: true, amount: 0.8 }}
            transition={{ duration: 0.5, delay: 0.3 }}
            variants={{
              hidden: { opacity: 0, y: 50 },
              visible: { opacity: 1, y: 0 },
            }}
            className="text-2xl text-center md:text-5xl font-bold"
          >
            {about}
          </motion.h1>
        </div>
      </div>
    </section>
  );
};

export default TopBanner;
