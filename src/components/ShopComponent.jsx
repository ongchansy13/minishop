import { useState } from "react";
import { motion } from "framer-motion";

import projuct1 from "../assets/product-1.png";
import projuct2 from "../assets/product-2.png";
import projuct3 from "../assets/product-3.png";
import projuct4 from "../assets/product-4.png";
import projuct5 from "../assets/product-5.png";
import projuct6 from "../assets/product-6.png";
import projuct7 from "../assets/product-7.png";
import projuct8 from "../assets/product-8.png";
import Pagnigation from "./Pagnigation";

const carts = [
  {
    cart: {
      productImg: projuct1,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct2,
      add: "ADD TO CART +",
      buy: "BUY NOW",
      discount: "50% off",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct3,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct4,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct5,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct6,
      add: "ADD TO CART +",
      buy: "BUY NOW",
      discount: "50% off",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct7,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct8,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
];

const ShopComponent = () => {
  const [isOpen1, setIsOpen1] = useState(false);
  const [isOpen2, setIsOpen2] = useState(false);
  const [isOpen3, setIsOpen3] = useState(false);
  const [isOpen4, setIsOpen4] = useState(false);

  return (
    <section className="py-12 bg-slate-50">
      <div className="max-w-[1200px] mx-auto grid md:grid-cols-[1fr,2fr]">
        <div className="md:justify-self-center px-20 md:px-5 py-5 row-start-2 md:row-span-full">
          <div>
            <motion.div
              initial="hidden"
              whileInView="visible"
              viewport={{ once: true, amount: 0.8 }}
              transition={{ duration: 0.5 }}
              variants={{
                hidden: { opacity: 0, y: 50 },
                visible: { opacity: 1, y: 0 },
              }}
              className="flex flex-col gap-7"
            >
              <h1 className="text-xl font-medium">CATEGORY</h1>
              <div>
                <div className="flex items-center gap-12 relative">
                  <motion.div

                    className=" ">
                    <p
                      className={`${isOpen1
                        ? "cursor-pointer text-yellow-500"
                        : "cursor-pointer "
                        }`}
                      onClick={() => setIsOpen1(!isOpen1)}
                    >
                      MEN&apos;S SHOES
                    </p>
                    {isOpen1 && (
                      <motion.div
                        initial="hidden"
                        whileInView="visible"
                        viewport={{ once: true, amount: 0.8 }}
                        transition={{ duration: 1 }}
                        variants={{
                          hidden: { opacity: 0 },
                          visible: { opacity: 1 },
                        }}
                        className={`${isOpen1
                          ? "h-auto cursor-pointer mt-3 ml-7 flex flex-col gap-2 text-black"
                          : "h-0 "
                          }`}
                      >
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Sport
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Casual
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Running
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Jondan
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Socer
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Football
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          LifeStyle
                        </p>
                      </motion.div>
                    )}
                  </motion.div>
                  <motion.div
                    initial="hidden"
                    whileInView="visible"
                    viewport={{ once: true, amount: 0.8 }}
                    transition={{ duration: 0.5, delay: 0.5 }}
                    variants={{
                      hidden: { opacity: 0 },
                      visible: { opacity: 1 },
                    }}
                    className="absolute top-1 right-1/2 md:-right-7"
                    onClick={() => setIsOpen1(!isOpen1)}
                  >
                    {isOpen1 ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className={`${isOpen1 ? "w-4 h-4 text-yellow-500" : "w-4 h-4 "
                          }`}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="m19.5 8.25-7.5 7.5-7.5-7.5"
                        />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-4 h-4"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M12 4.5v15m7.5-7.5h-15"
                        />
                      </svg>
                    )}
                  </motion.div>
                </div>
              </div>

              <div>
                <div className="flex items-center gap-12 relative">
                  <div>
                    <p
                      className={`${isOpen2
                        ? "cursor-pointer text-yellow-500"
                        : "cursor-pointer "
                        }`}
                      onClick={() => setIsOpen2(!isOpen2)}
                    >
                      WOMEN&apos;S SHOES
                    </p>
                    {isOpen2 && (
                      <motion.div
                        initial="hidden"
                        whileInView="visible"
                        viewport={{ once: true, amount: 0.8 }}
                        transition={{ duration: 1 }}
                        variants={{
                          hidden: { opacity: 0 },
                          visible: { opacity: 1 },
                        }}
                        className={`${isOpen2
                          ? "h-auto cursor-pointer mt-3 ml-7 flex flex-col gap-2 text-black"
                          : "h-0"
                          }`}
                      >
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Sport
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Casual
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Running
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Jondan
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Socer
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Football
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          LifeStyle
                        </p>
                      </motion.div>
                    )}
                  </div>
                  <div
                    className="absolute top-1 right-1/2 md:-right-7"
                    onClick={() => setIsOpen2(!isOpen2)}
                  >
                    {isOpen2 ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className={`${isOpen2 ? "w-4 h-4 text-yellow-500" : "w-4 h-4 "
                          }`}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="m19.5 8.25-7.5 7.5-7.5-7.5"
                        />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-4 h-4"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M12 4.5v15m7.5-7.5h-15"
                        />
                      </svg>
                    )}
                  </div>
                </div>
              </div>

              <div>
                <div className="flex items-center gap-12 relative">
                  <div>
                    <p
                      className={`${isOpen3
                        ? "cursor-pointer text-yellow-500"
                        : "cursor-pointer "
                        }`}
                      onClick={() => setIsOpen3(!isOpen3)}
                    >
                      ACCESSORIES
                    </p>
                    {isOpen3 && (
                      <motion.div
                        initial="hidden"
                        whileInView="visible"
                        viewport={{ once: true, amount: 0.8 }}
                        transition={{ duration: 1 }}
                        variants={{
                          hidden: { opacity: 0 },
                          visible: { opacity: 1 },
                        }}
                        className={`${isOpen3
                          ? "h-auto cursor-pointer mt-3 ml-7 flex flex-col gap-2 text-black"
                          : "h-0"
                          }`}
                      >
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Jeans
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          T-Shirt
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Jacket
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Shoes
                        </p>
                      </motion.div>
                    )}
                  </div>
                  <div
                    className="absolute  top-1 right-1/2 md:-right-7"
                    onClick={() => setIsOpen3(!isOpen3)}
                  >
                    {isOpen3 ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className={`${isOpen3 ? "w-4 h-4 text-yellow-500" : "w-4 h-4 "
                          }`}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="m19.5 8.25-7.5 7.5-7.5-7.5"
                        />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-4 h-4"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M12 4.5v15m7.5-7.5h-15"
                        />
                      </svg>
                    )}
                  </div>
                </div>
              </div>

              <div>
                <div className="flex items-center gap-12 relative">
                  <div>
                    <p
                      className={`${isOpen4
                        ? "cursor-pointer text-yellow-500"
                        : "cursor-pointer "
                        }`}
                      onClick={() => setIsOpen4(!isOpen4)}
                    >
                      CLOTHING
                    </p>
                    {isOpen4 && (
                      <motion.div
                        initial="hidden"
                        whileInView="visible"
                        viewport={{ once: true, amount: 0.8 }}
                        transition={{ duration: 1 }}
                        variants={{
                          hidden: { opacity: 0 },
                          visible: { opacity: 1 },
                        }}
                        className={`${isOpen4
                          ? "h-auto cursor-pointer mt-3 ml-7 flex flex-col gap-2 text-black"
                          : "h-0"
                          }`}
                      >
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Jeans
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          T-Shirt
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Jacket
                        </p>
                        <p className="text-sm hover:text-yellow-500 transition-all duration-300">
                          Shoes
                        </p>
                      </motion.div>
                    )}
                  </div>
                  <div
                    className="absolute top-1 right-1/2 md:-right-7"
                    onClick={() => setIsOpen4(!isOpen4)}
                  >
                    {isOpen4 ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className={`${isOpen4 ? "w-4 h-4 text-yellow-500" : "w-4 h-4 "
                          }`}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="m19.5 8.25-7.5 7.5-7.5-7.5"
                        />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-4 h-4"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M12 4.5v15m7.5-7.5h-15"
                        />
                      </svg>
                    )}
                  </div>
                </div>
              </div>
            </motion.div>

            <motion.div
              initial="hidden"
              whileInView="visible"
              viewport={{ once: true, amount: 0.8 }}
              transition={{ duration: 0.5 }}
              variants={{
                hidden: { opacity: 0, y: 50 },
                visible: { opacity: 1, y: 0 },
              }}
              className="mt-7 flex flex-col gap-5"
            >
              <h1>PRICE RANGE</h1>
              <div className="flex flex-col gap-5 ">
                <div className="flex flex-col gap-2">
                  <p>Price from:</p>
                  <select
                    type="text"
                    className="p-3 w-1/2 lg:w-full outline outline-1 select-none"
                    placeholder="1"
                  >
                    <option value="">1</option>
                    <option value="">100</option>
                    <option value="">200</option>
                    <option value="">300</option>
                    <option value="">1000</option>
                  </select>
                </div>

                <div className="flex flex-col gap-2">
                  <p>Price from:</p>
                  <select
                    type="text"
                    className="p-3 w-1/2 lg:w-full outline outline-1 select-none"
                    placeholder="200"
                  >
                    <option value="">200</option>
                    <option value="">400</option>
                    <option value="">600</option>
                    <option value="">800</option>
                    <option value="">1000</option>
                  </select>
                </div>
              </div>
            </motion.div>
          </div>
        </div>

        <div className="row-start-1 md:row-span-full">
          <section className="max-w-[1200px] mx-auto px-5 grid lg:grid-cols-2 xl:grid-cols-3 items-center justify-center gap-5">
            {carts.slice(0, 8).map((c, index) => {
              return (
                <section key={index}>
                  <motion.div
                    initial="hidden"
                    whileInView="visible"
                    viewport={{ once: true, amount: 0.8 }}
                    transition={{ duration: 0.5 }}
                    variants={{
                      hidden: { opacity: 0, y: 50 },
                      visible: { opacity: 1, y: 0 },
                    }}
                  >
                    <div className="shadow-sm bg-slate-300">
                      <div className="group relative cursor-pointer">
                        <div className=" ">
                          <div className="overflow-hidden relative  ">
                            <div className="group-hover:opacity-100 opacity-0 absolute top-[50%] left-[50%] -translate-x-[50%] -translate-y-[50%] h-[300px] w-[300px] md:w-[220px] md:h-[220px] group-hover:bg-pink-500 rounded-full"></div>
                            <img
                              className=" transition-all duration-150 group-hover:scale-75 md:group-hover:scale-125  w-full"
                              src={c.cart.productImg}
                              alt=""
                            />
                          </div>
                        </div>

                        <div className="opacity-0   group-hover:opacity-100 flex items-center justify-center  transition-opacity duration-150 z-40 absolute md:top-[85%] md:left-[5%] md:-translate-x-0 md:-translate-y-0 -translate-x-[50%] -translate-y-[50%] top-[90%] left-[45%] ">
                          <div className="bg-black hover:bg-yellow-600 text-white p-2  transition-all duration-300">
                            <p className="text-sm">{c.cart.add}</p>
                          </div>

                          <div className="flex items-center justify-center gap-2 p-2 bg-slate-200  hover:bg-yellow-600 transition-all duration-300">
                            <p className="text-sm">{c.cart.buy}</p>
                            <ion-icon name="cart"></ion-icon>
                          </div>
                        </div>
                        {index === 1 && (
                          <p className="bg-yellow-500 uppercase text-md p-2 absolute -rotate-90 top-5 -left-5">
                            50% off
                          </p>
                        )}
                        {index === 5 && (
                          <p className="bg-yellow-500 uppercase text-md p-2 absolute -rotate-90 top-5 -left-5">
                            50% off
                          </p>
                        )}
                      </div>
                      <div className="bg-white p-2">
                        <div className="flex items-center justify-between mb-2">
                          <p>{c.footer.title}</p>
                          <div>
                            <ion-icon
                              style={{ color: "orange" }}
                              name="star-outline"
                            ></ion-icon>
                            <ion-icon
                              style={{ color: "orange" }}
                              name="star-outline"
                            ></ion-icon>
                            <ion-icon
                              style={{ color: "orange" }}
                              name="star-outline"
                            ></ion-icon>
                            <ion-icon
                              style={{ color: "orange" }}
                              name="star-outline"
                            ></ion-icon>
                            <ion-icon
                              style={{ color: "orange" }}
                              name="star-outline"
                            ></ion-icon>
                          </div>
                        </div>
                        <div className="mb-2">
                          <h5 className="font-bold">{c.footer.desc}</h5>
                        </div>
                        <div>
                          <p className="font-thin">{c.footer.price}</p>
                        </div>
                      </div>
                    </div>
                  </motion.div>
                </section>
              );
            })}
          </section>
          <div className="flex justify-center py-12">
            <Pagnigation />
          </div>
        </div>
      </div>
    </section>
  );
};

export default ShopComponent;
