import { motion } from "framer-motion";

const Service = () => {
  return (
    <>
      <motion.section
        initial="hidden"
        whileInView="visible"
        viewport={{ once: true, amount: 0.8 }}
        transition={{ duration: 0.5 }}
        variants={{
          hidden: { opacity: 0, y: 50 },
          visible: { opacity: 1, y: 0 },
        }}
        className={`py-10 px-10`}
      >
        <div className="max-w-[1200px] mx-auto grid gap-7 md:grid-cols-3 items-center place-items-center pt-20 md:p-5 ">
          <div className="grid items-center gap-5 justify-center place-items-center">
            <ion-icon
              style={{ fontSize: "60px" }}
              name="gift-outline"
            ></ion-icon>
            <h5 className="text-center font-bold text-lg">Free Shipping</h5>
            <p className="text-center text-slate-400 text-sm">
              Far far away, behind the word mountains, far from the countries
              Vokalia and Consonantia, there live the blind texts.
            </p>
          </div>
          <div className="grid items-center gap-5 justify-center  place-items-center">
            <ion-icon
              style={{ fontSize: "60px" }}
              name="people-outline"
            ></ion-icon>
            <h4 className="text-center font-bold text-lg">Support Customer</h4>
            <p className="text-center text-slate-400 text-sm">
              Far far away, behind the word mountains, far from the countries
              Vokalia and Consonantia, there live the blind texts.
            </p>
          </div>
          <div className="grid items-center gap-5 justify-center  place-items-center">
            <ion-icon
              style={{ fontSize: "60px" }}
              name="card-outline"
            ></ion-icon>

            <h4 className="text-center font-bold text-lg">Secure Payments</h4>
            <p className="text-center text-slate-400 text-sm">
              Far far away, behind the word mountains, far from the countries
              Vokalia and Consonantia, there live the blind texts.
            </p>
          </div>
        </div>
      </motion.section>
    </>
  );
};

export default Service;
