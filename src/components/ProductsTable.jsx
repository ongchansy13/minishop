import Product4 from "../assets/product-4.png";
import Product5 from "../assets/prod-1.png";
const ProductsTable = () => {
  return (
    <>
      <section className="py-12 overflow-x-auto scrollbar scrollbar-thumb-rounded scrollbar-track-gray-100 scrollbar-thumb-gray-400 ">
        <div className=" w-[1200px]  mx-auto p-5 ">
          <table className="table-auto overflow-hidden">
            <thead className="w-full bg-orange-300 ">
              <tr>
                <th className="text-center p-5"></th>
                <th className="text-center p-5"></th>
                <th className="tetext-center p-5 ">Product</th>
                <th className="text-center p-5">Price</th>
                <th className="text-center p-5">Quantity</th>
                <th className="text-center p-5">Total</th>
              </tr>
            </thead>

            <tbody className="bg-white shadow-sm">
              <tr className="border-b">
                <td className="text-center p-5 ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-6 h-6 bg-yellow-500 cursor-pointer"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M6 18 18 6M6 6l12 12"
                    />
                  </svg>
                </td>
                <td className="text-center p-5 ">
                  <img className="w-32" src={Product4} alt="" />
                </td>
                <td className="p-5 text-center ">
                  <h3 className="text-center mb-2 text-md font-bold">
                    NIKE FREE RN 2019 ID
                  </h3>
                  <p className="text-center text-sm ">
                    Far far away, behind the word mountains, far from the
                    countries
                  </p>
                </td>
                <td className="p-5 ">$15.70</td>
                <td className="p-5 ">
                  <input
                    type="text"
                    className="w-32 text-center"
                    placeholder="1"
                  />
                </td>
                <td className="p-5 ">$15.70</td>
              </tr>

              <tr className="border-b">
                <td className="text-center p-5 ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-6 h-6 bg-yellow-500 cursor-pointer"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M6 18 18 6M6 6l12 12"
                    />
                  </svg>
                </td>
                <td className="text-center p-5 ">
                  <img className="w-52" src={Product5} alt="" />
                </td>
                <td className="p-5 text-center ">
                  <h3 className="text-center mb-2 text-md font-bold">
                    NIKE FREE RN 2019 ID
                  </h3>
                  <p className="text-center text-sm ">
                    Far far away, behind the word mountains, far from the
                    countries
                  </p>
                </td>
                <td className="p-5 ">$15.70</td>
                <td className="p-5 ">
                  <input
                    type="text"
                    className="w-32 text-center"
                    placeholder="1"
                  />
                </td>
                <td className="p-5 ">$15.70</td>
              </tr>
            </tbody>
          </table>
        </div>
      </section>

      <div className="max-w-[1200px] mx-auto px-5 py-5">
        <div className="flex flex-col gap-5 py-5 px-10 border bg-white md:w-2/5">
          <div>
            <h1 className="font-bold text-md md:text-xl">CART TOTALS</h1>
          </div>

          <div className="flex items-center gap-12">
            <div>Subtotal</div>
            <div>$20.60</div>
          </div>
          <div className="flex items-center gap-12">
            <div>Subtotal</div>
            <div>$20.60</div>
          </div>
          <div className="flex items-center gap-12 pb-2 border-b">
            <div>Subtotal</div>
            <div>$20.60</div>
          </div>
          <div className="flex items-center gap-16">
            <div className="font-bold text-lg">Total</div>
            <div className="font-bold text-lg">$20.60</div>
          </div>
        </div>
        <button className="w-full md:w-auto md:flex items-center gap-16 py-5 px-10 mt-5 rounded-full bg-orange-300 text-white hover:bg-white hover:border-orange-300 hover:text-orange-300 hover:border transition-all ease-in-out duration-150 ">
          <a href="#">Procced to checkout</a>
        </button>
      </div>
    </>
  );
};

export default ProductsTable;
