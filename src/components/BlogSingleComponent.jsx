import person1 from '../assets/person_1.jpg';

import event1 from "../assets/event1.jpg";
import event2 from "../assets/event2.jpg";
import event3 from "../assets/event3.jpg";
// import event4 from '../assets/event4.jpg';
// import event5 from '../assets/event5.jpg';

const eventsList = [
  {
    img: event1,
    title: "8 Tips For Shopping",
    desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, eius mollitia suscipit, quisquam doloremque distinctio perferendis et doloribus unde architecto optio laboriosam porro adipisci sapiente officiis nemo accusamus ad praesentium? Esse minima nisi et. Dolore perferendis, enim praesentium omnis, iste doloremque quia officia optio deserunt molestiae voluptates soluta architecto tempora.",
    detail:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, eius mollitia suscipit, quisquam doloremque distinctio perferendis et doloribus unde architecto optio laboriosam porro adipisci sapiente officiis nemo accusamus ad praesentium? Esse minima nisi et. Dolore perferendis, enim praesentium omnis, iste doloremque quia officia optio deserunt molestiae voluptates soluta architecto tempora.",
  },
  {
    img: event2,
    title: "#2. Creative WordPress Themes",
    desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, eius mollitia suscipit, quisquam doloremque distinctio perferendis et doloribus unde architecto optio laboriosam porro adipisci sapiente officiis nemo accusamus ad praesentium? Esse minima nisi et. Dolore perferendis, enim praesentium omnis, iste doloremque quia officia optio deserunt molestiae voluptates soluta architecto tempora.",
    detail1:
      `Quisquam esse aliquam fuga distinctio, quidem delectus veritatis reiciendis. Nihil explicabo quod, est eos ipsum. Unde aut non tenetur tempore, nisi culpa voluptate maiores officiis quis vel ab consectetur suscipit veritatis nulla quos quia aspernatur perferendis, libero sint. Error, velit, porro. Deserunt minus, quibusdam iste enim veniam, modi rem maiores.`,
    detail2:
      `Quisquam esse aliquam fuga distinctio, quidem delectus veritatis reiciendis. Nihil explicabo quod, est eos ipsum. Unde aut non tenetur tempore, nisi culpa voluptate maiores officiis quis vel ab consectetur suscipit veritatis nulla quos quia aspernatur perferendis, libero sint. Error, velit, porro. Deserunt minus, quibusdam iste enim veniam, modi rem maiores.`,
    detail3:
      `Quisquam esse aliquam fuga distinctio, quidem delectus veritatis reiciendis. Nihil explicabo quod, est eos ipsum. Unde aut non tenetur tempore, nisi culpa voluptate maiores officiis quis vel ab consectetur suscipit veritatis nulla quos quia aspernatur perferendis, libero sint. Error, velit, porro. Deserunt minus, quibusdam iste enim veniam, modi rem maiores.`,
  },
];

const subEvent = [
  {
    img: event1,
    desc: "Even the all-powerful Pointing has no control about the blind texts",
  },
  {
    img: event2,
    desc: "Even the all-powerful Pointing has no control about the blind texts",
  },
  {
    img: event3,
    desc: "Even the all-powerful Pointing has no control about the blind texts",
  },
];

const BlogSingleComponent = () => {
  return (
    <>
      <section className="lg:py-24 py-12 ">
        <div className="max-w-[1200px] mx-auto px-5 grid lg:grid-cols-[2fr,1fr] gap-12 ">
          <div className="lg:justify-self-start lg:row-span-full row-start-1">
            <div className="flex flex-col gap-12">
              {eventsList.map((event, index) => (
                <div key={index}>
                  <div className="grid gap-5">
                    <div>
                      <h1 className="text-2xl md:text-3xl">{event.title}</h1>
                      <p className="mt-5">{event.desc}</p>
                    </div>

                    <div className="">
                      <img className="w-full h-auto " src={event.img} alt="" />
                    </div>

                    <div>
                      <p className="mb-2">{event.detail1}</p>
                      <p className="mb-2">{event.detail2}</p>
                      <p className="mb-2">{event.detail2}</p>
                      <p>{event.detail3}</p>
                    </div>
                  </div>
                </div>
              ))}
            </div>

            <div className="flex flex-col mt-10 gap-5">
              <h1>TAG CLOUD</h1>
              <div className="flex items-center gap-2 flex-wrap">
                <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                  Life
                </div>
                <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                  Sport
                </div>
                <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                  Tech
                </div>
                <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                  Travel
                </div>

              </div>
            </div>


            <div className="flex gap-5 p-5 items-center mt-10 bg-slate-300">
              <div>
                <img className='w-full h-auto' src={person1} alt="" />
              </div>
              <div className='flex flex-col gap-4'>
                <h1 className='text-3xl'>Lance Smith</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
              </div>
            </div>

            <div >
              <h1 className='my-10 text-3xl'>6 Comments</h1>


              <div>
                <div className="flex gap-5 mt-10">
                  <div className='w-32 h-32 '>
                    <img className='w-full h-auto rounded-full ' src={person1} alt="" />
                  </div>
                  <div className='flex flex-col gap-4'>
                    <h1 className='text-3xl'>Lance Smith</h1>
                    <p className='text-sm opacity-45'>JUNE 27, 2018 AT 2:21PM</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>

                  </div>

                </div>
                <button className='ml-16 rounded-md mt-5 px-2 py-1 bg-gray-100'>
                  <a href="#" className=''>Reply</a>
                </button>
              </div>

              <div>
                <div className="flex gap-5 mt-10">
                  <div className='w-32 h-32 '>
                    <img className='w-full h-auto rounded-full ' src={person1} alt="" />
                  </div>
                  <div className='flex flex-col gap-4'>
                    <h1 className='text-3xl'>Lance Smith</h1>
                    <p className='text-sm opacity-45'>JUNE 27, 2018 AT 2:21PM</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>

                  </div>

                </div>
                <button className='ml-16 rounded-md mt-5 px-2 py-1 bg-gray-100'>
                  <a href="#" className=''>Reply</a>
                </button>
              </div>

              <div className='ml-20'>
                <div className="flex gap-5 mt-10">
                  <div className='w-32 h-32 '>
                    <img className='w-full h-auto rounded-full ' src={person1} alt="" />
                  </div>
                  <div className='flex flex-col gap-4'>
                    <h1 className='text-3xl'>Lance Smith</h1>
                    <p className='text-sm opacity-45'>JUNE 27, 2018 AT 2:21PM</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>

                  </div>

                </div>
                <button className='ml-16 rounded-md mt-5 px-2 py-1 bg-gray-100'>
                  <a href="#" className=''>Reply</a>
                </button>
              </div>

              <div className='ml-28'>
                <div className="flex gap-5 mt-10">
                  <div className='w-32 h-32 '>
                    <img className='w-full h-auto rounded-full ' src={person1} alt="" />
                  </div>
                  <div className='flex flex-col gap-4'>
                    <h1 className='text-3xl'>Lance Smith</h1>
                    <p className='text-sm opacity-45'>JUNE 27, 2018 AT 2:21PM</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>

                  </div>

                </div>
                <button className='ml-16 rounded-md mt-5 px-2 py-1 bg-gray-100'>
                  <a href="#" className=''>Reply</a>
                </button>
              </div>

              <div className='ml-32'>
                <div className="flex gap-5 mt-10">
                  <div className='w-32 h-32 '>
                    <img className='w-full h-auto rounded-full ' src={person1} alt="" />
                  </div>
                  <div className='flex flex-col gap-4'>
                    <h1 className='text-3xl'>Lance Smith</h1>
                    <p className='text-sm opacity-45'>JUNE 27, 2018 AT 2:21PM</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>

                  </div>

                </div>
                <button className='ml-16 rounded-md mt-5 px-2 py-1 bg-gray-100'>
                  <a href="#" className=''>Reply</a>
                </button>
              </div>

              <div className='mt-20'>
                <div className="flex gap-5 mt-10">
                  <div className='w-32 h-32 '>
                    <img className='w-full h-auto rounded-full ' src={person1} alt="" />
                  </div>
                  <div className='flex flex-col gap-4'>
                    <h1 className='text-3xl'>Lance Smith</h1>
                    <p className='text-sm opacity-45'>JUNE 27, 2018 AT 2:21PM</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>

                  </div>

                </div>
                <button className='ml-16 rounded-md mt-5 px-2 py-1 bg-gray-100'>
                  <a href="#" className=''>Reply</a>
                </button>
              </div>
            </div>

            <div>
              <h1 className='my-10 text-3xl'>Leave a comment</h1>

              <div className='p-10 flex flex-col gap-2 bg-gray-100 shadow-sm'>
                <div className='flex flex-col gap-5'>
                  <label>Name *</label>
                  <input className='p-4' type="text" />
                </div>
                <div className='flex flex-col gap-5'>
                  <label>Email *</label>
                  <input className='p-4' type="text" />
                </div>
                <div className='flex flex-col gap-5'>
                  <label>Website</label>
                  <input className='p-4' type="text" />
                </div>
                <div className='flex flex-col gap-5'>
                  <label>Message</label>
                  <textarea cols="30" rows="10"></textarea>
                </div>

                <div>
                  <button className='bg-yellow-500 px-5 py-3 rounded-full opacity-50 hover:bg-white hover:outline hover:outline-1 hover:outline-yellow-500 transition-all duration-200'>Post comment</button>
                </div>
              </div>
            </div>

          </div>

          <div className="lg:justify-self-end lg:row-span-full row-start-2">
            <div className="flex flex-col  gap-12">
              <div className="flex items-center w-full  bg-none border border-black px-5 py-2 hover:border-orange-800  ">
                <input
                  type="text"
                  className=" border-none outline-none  bg-none placeholder:text-sm  w-full "
                  placeholder="TYPE A KEYWORD AND HIT ENTER"
                />
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  className="w-4 h-4"
                >
                  <path
                    fillRule="evenodd"
                    d="M10.5 3.75a6.75 6.75 0 1 0 0 13.5 6.75 6.75 0 0 0 0-13.5ZM2.25 10.5a8.25 8.25 0 1 1 14.59 5.28l4.69 4.69a.75.75 0 1 1-1.06 1.06l-4.69-4.69A8.25 8.25 0 0 1 2.25 10.5Z"
                    clipRule="evenodd"
                  />
                </svg>
              </div>

              <div className="flex flex-col gap-7">
                <h1 className="font-medium">CATEGORIES</h1>
                <div className="flex flex-col gap-5">
                  <div className="flex items-center justify-between  border-b pb-2">
                    <p>Shoes</p>
                    <p className="opacity-25 font-medium">(12)</p>
                  </div>
                  <div className="flex items-center justify-between  border-b pb-2">
                    <p>Men&apos; Shoes</p>
                    <p className="opacity-25 font-medium">(22)</p>
                  </div>
                  <div className="flex items-center justify-between  border-b pb-2">
                    <p>Women&apos;s</p>
                    <p className="opacity-25 font-medium">(37)</p>
                  </div>
                  <div className="flex items-center justify-between  border-b pb-2">
                    <p>Accessories</p>
                    <p className="opacity-25 font-medium">(42)</p>
                  </div>
                  <div className="flex items-center justify-between  border-b pb-2">
                    <p>Sports</p>
                    <p className="opacity-25 font-medium">(14)</p>
                  </div>
                  <div className="flex items-center justify-between">
                    <p>Lifestyle</p>
                    <p className="opacity-25 font-medium">(140)</p>
                  </div>
                </div>
              </div>

              <div className="flex flex-col gap-5">
                {subEvent.map((subevent, index) => (
                  <div key={index}>
                    <div className="grid grid-cols-[1fr,2fr]  gap-7">
                      <div className="">
                        <img
                          className="w-full h-auto "
                          src={subevent.img}
                          alt=""
                        />
                      </div>
                      <div className="flex flex-col gap-7">
                        <p className=" text-gray-500 text-xl font-thin">
                          {subevent.desc}
                        </p>
                        <div className="flex items-center gap-1 text-sm opacity-50">
                          <div>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 24 24"
                              fill="currentColor"
                              className="w-3 h-3"
                            >
                              <path d="M12.75 12.75a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM7.5 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM8.25 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM9.75 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM10.5 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM12 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM12.75 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM14.25 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM15 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM16.5 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM15 12.75a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM16.5 13.5a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5Z" />
                              <path
                                fillRule="evenodd"
                                d="M6.75 2.25A.75.75 0 0 1 7.5 3v1.5h9V3A.75.75 0 0 1 18 3v1.5h.75a3 3 0 0 1 3 3v11.25a3 3 0 0 1-3 3H5.25a3 3 0 0 1-3-3V7.5a3 3 0 0 1 3-3H6V3a.75.75 0 0 1 .75-.75Zm13.5 9a1.5 1.5 0 0 0-1.5-1.5H5.25a1.5 1.5 0 0 0-1.5 1.5v7.5a1.5 1.5 0 0 0 1.5 1.5h13.5a1.5 1.5 0 0 0 1.5-1.5v-7.5Z"
                                clipRule="evenodd"
                              />
                            </svg>
                          </div>
                          <div>April 27,2019</div>
                          <div>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              viewBox="0 0 24 24"
                              strokeWidth={1.5}
                              stroke="currentColor"
                              className="w-3 h-3"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M15.75 6a3.75 3.75 0 1 1-7.5 0 3.75 3.75 0 0 1 7.5 0ZM4.501 20.118a7.5 7.5 0 0 1 14.998 0A17.933 17.933 0 0 1 12 21.75c-2.676 0-5.216-.584-7.499-1.632Z"
                              />
                            </svg>
                          </div>
                          <div>Admin</div>
                          <div>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 24 24"
                              fill="currentColor"
                              className="w-3 h-3"
                            >
                              <path
                                fillRule="evenodd"
                                d="M4.848 2.771A49.144 49.144 0 0 1 12 2.25c2.43 0 4.817.178 7.152.52 1.978.292 3.348 2.024 3.348 3.97v6.02c0 1.946-1.37 3.678-3.348 3.97a48.901 48.901 0 0 1-3.476.383.39.39 0 0 0-.297.17l-2.755 4.133a.75.75 0 0 1-1.248 0l-2.755-4.133a.39.39 0 0 0-.297-.17 48.9 48.9 0 0 1-3.476-.384c-1.978-.29-3.348-2.024-3.348-3.97V6.741c0-1.946 1.37-3.68 3.348-3.97ZM6.75 8.25a.75.75 0 0 1 .75-.75h9a.75.75 0 0 1 0 1.5h-9a.75.75 0 0 1-.75-.75Zm.75 2.25a.75.75 0 0 0 0 1.5H12a.75.75 0 0 0 0-1.5H7.5Z"
                                clipRule="evenodd"
                              />
                            </svg>
                          </div>
                          <div>19</div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>

              <div className="flex flex-col  gap-5">
                <h1>TAG CLOUD</h1>
                <div className="flex items-center gap-2 flex-wrap">
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    Shoes
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    Products
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    shirt
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    jeans
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    shop
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    dress
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    coats
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    jusmpsuits
                  </div>
                </div>
              </div>

              <div className="flex flex-col  gap-5">
                <h1 className="font-meduim">PARAGRAPH</h1>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Ducimus itaque, autem necessitatibus voluptate quod mollitia
                  delectus aut, sunt placeat nam vero culpa sapiente consectetur
                  similique, inventore eos fugit cupiditate numquam!
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default BlogSingleComponent;
