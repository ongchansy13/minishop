// import { useState } from "react";
import { Link, Outlet } from "react-router-dom";


import { useState, useEffect } from "react";
import HeaderContact from "./HeaderContact";


const Header = () => {
  const [showNavBar, setShowNavBar] = useState('');
  const [contact, setContact] = useState(true);

  const [dromdown, setDropdown] = useState(false);
  const [toggle, setToggle] = useState(false);
  const [showNav, setShowNav] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 250) {
        setShowNavBar('bg-white text-black fixed top-0 md:shadow-lg z-20');
        setContact(false);
      } else {
        setShowNavBar('md:bg-transparent bg-black text-white z-20');
        setContact(true);
      }
    }
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);




  return (
    <>
      <header className={` w-full  ${showNavBar}`}>
        <div className={`bg-black text-white mb-5 ${contact ? 'block' : 'hidden'}`}>

          <HeaderContact />
        </div>

        <nav className='max-w-[1200px] mx-auto px-5 pb-5 flex md:items-center justify-between cursor-pointer'>

          <div className="">
            <p className='text-cyan-500  font-bold my-5 md:my-0'>Mini Shop</p>

            <div>
              <ul className={`flex flex-col gap-2 py-2 md:hidden ${showNav ? 'flex' : 'hidden'}`} >
                <li>
                  <Link className='text-cyan-500' to="/">Home</Link>
                </li>
                <li className={``}>
                  <div className=''>
                    <div className=' goup  transition-colors duration-100'
                      onClick={() => setDropdown(!dromdown)}
                    >Category
                      <div className={`flex flex-col ${dromdown ? 'h-auto' : 'hidden'}`}>
                        <Link className='hover:bg-blue-500 group-hover:text-white p-2 hover:text-white transition-all duration-100' to="/shop">Shop</Link>
                        <Link className='hover:bg-blue-500 group-hover:text-white p-2 hover:text-white transition-all duration-100' to="/singleproduct">Single Product</Link>
                        <Link className='hover:bg-blue-500 group-hover:text-white p-2 hover:text-white transition-all duration-100' to="/checkout">Checkout</Link>
                        <Link className='hover:bg-blue-500 group-hover:text-white p-2 hover:text-white transition-all duration-100' to="/cart">Cart</Link>
                        <Link className='hover:bg-blue-500 group-hover:text-white p-2 hover:text-white transition-all duration-100' to="/checkout">Checkout</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className={``}>
                  <Link className='hover:text-cyan-500 transition-colors duration-100' to='/about'>About</Link>
                </li>
                <li className={``}>
                  <Link className='hover:text-cyan-500 transition-colors duration-100' to='/blog'>Blog</Link>
                </li>
                <li className={``}>
                  <Link className='hover:text-cyan-500 transition-colors duration-100' to="/contact">Contact</Link>
                </li>
                <li className={``}>
                  <Link className='hover:text-cyan-500  transition-colors duration-100' to="/cart">
                    <div className="flex items-center  ">

                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-5 h-5">
                        <path d="M2.25 2.25a.75.75 0 0 0 0 1.5h1.386c.17 0 .318.114.362.278l2.558 9.592a3.752 3.752 0 0 0-2.806 3.63c0 .414.336.75.75.75h15.75a.75.75 0 0 0 0-1.5H5.378A2.25 2.25 0 0 1 7.5 15h11.218a.75.75 0 0 0 .674-.421 60.358 60.358 0 0 0 2.96-7.228.75.75 0 0 0-.525-.965A60.864 60.864 0 0 0 5.68 4.509l-.232-.867A1.875 1.875 0 0 0 3.636 2.25H2.25ZM3.75 20.25a1.5 1.5 0 1 1 3 0 1.5 1.5 0 0 1-3 0ZM16.5 20.25a1.5 1.5 0 1 1 3 0 1.5 1.5 0 0 1-3 0Z" />
                      </svg>
                      <p className="text-[10px]">[0]</p>
                    </div>
                  </Link>
                </li>

              </ul>
            </div>
          </div>

          <ul className='md:flex mt-2 items-center gap-10 uppercase font-medium text-md hidden '>
            <li className="text-black">
              <Link className='text-cyan-500' to="/">Home</Link>
            </li>
            <li className="text-black">
              <div className='relative'>
                <p className='hover:text-cyan-500 transition-colors duration-100'
                  onClick={() => setDropdown(!dromdown)}
                >Category</p>
                <div className={`flex flex-col bg-white z-20 text-black p-2 w-[200px] shadow-md absolute top-8 -left-5  ${dromdown ? 'h-auto' : 'hidden'}`}>
                  <Link className='hover:bg-blue-500 p-2 hover:text-white transition-all duration-100' to="/shop">Shop</Link>
                  <Link className='hover:bg-blue-500 p-2 hover:text-white transition-all duration-100' to="/singleproduct">Single Product</Link>
                  <Link className='hover:bg-blue-500 p-2 hover:text-white transition-all duration-100' to="/cart">Cart</Link>
                  <Link className='hover:bg-blue-500 p-2 hover:text-white transition-all duration-100' to="/checkout">Checkout</Link>
                </div>
              </div>
            </li>
            <li className="text-black">
              <Link className='hover:text-cyan-500 transition-colors duration-100' to='/about'>About</Link>
            </li>
            <li className="text-black ">
              <Link className='hover:text-cyan-500 transition-colors duration-100' to="/blog">Blog</Link>
            </li>
            <li className="text-black ">
              <Link className='hover:text-cyan-500 transition-colors duration-100' to="/contact">Contact</Link>
            </li>
            <li className="text-black ">
              <Link className='hover:text-cyan-500 transition-colors duration-100' to="/cart">

                <div className="flex items-center ">

                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-5 h-5">
                    <path d="M2.25 2.25a.75.75 0 0 0 0 1.5h1.386c.17 0 .318.114.362.278l2.558 9.592a3.752 3.752 0 0 0-2.806 3.63c0 .414.336.75.75.75h15.75a.75.75 0 0 0 0-1.5H5.378A2.25 2.25 0 0 1 7.5 15h11.218a.75.75 0 0 0 .674-.421 60.358 60.358 0 0 0 2.96-7.228.75.75 0 0 0-.525-.965A60.864 60.864 0 0 0 5.68 4.509l-.232-.867A1.875 1.875 0 0 0 3.636 2.25H2.25ZM3.75 20.25a1.5 1.5 0 1 1 3 0 1.5 1.5 0 0 1-3 0ZM16.5 20.25a1.5 1.5 0 1 1 3 0 1.5 1.5 0 0 1-3 0Z" />
                  </svg>
                  <p className="text-[10px]">[0]</p>
                </div>
              </Link>
            </li>


          </ul>


          <div
            className='flex mt-5 md:hidden'
            onClick={() => setToggle((prev) => !prev)}
          >
            <div
              onClick={() => setShowNav((prev) => !prev)}
            >
              {
                toggle ? (<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6">
                  <path fillRule="evenodd" d="M5.47 5.47a.75.75 0 0 1 1.06 0L12 10.94l5.47-5.47a.75.75 0 1 1 1.06 1.06L13.06 12l5.47 5.47a.75.75 0 1 1-1.06 1.06L12 13.06l-5.47 5.47a.75.75 0 0 1-1.06-1.06L10.94 12 5.47 6.53a.75.75 0 0 1 0-1.06Z" clipRule="evenodd" />
                </svg>
                ) : (
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6 ">
                    <path fillRule="evenodd" d="M3 6.75A.75.75 0 0 1 3.75 6h16.5a.75.75 0 0 1 0 1.5H3.75A.75.75 0 0 1 3 6.75ZM3 12a.75.75 0 0 1 .75-.75h16.5a.75.75 0 0 1 0 1.5H3.75A.75.75 0 0 1 3 12Zm0 5.25a.75.75 0 0 1 .75-.75h16.5a.75.75 0 0 1 0 1.5H3.75a.75.75 0 0 1-.75-.75Z" clipRule="evenodd" />
                  </svg>
                )
              }
            </div>
          </div>



        </nav>

      </header>

      <main className="">
        <Outlet />
      </main>
    </>
  );
};

export default Header;
