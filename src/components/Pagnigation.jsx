const Pagnigation = () => {
  return (
    <section>
      <div className="flex items-center gap-1">
        <div className="p-3 rounded-full border">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className="w-6  h-6 "
          >
            <path
              fillRule="evenodd"
              d="M7.72 12.53a.75.75 0 0 1 0-1.06l7.5-7.5a.75.75 0 1 1 1.06 1.06L9.31 12l6.97 6.97a.75.75 0 1 1-1.06 1.06l-7.5-7.5Z"
              clipRule="evenodd"
            />
          </svg>
        </div>

        <div className="px-5 py-3 rounded-full border bg-black text-white">
          1
        </div>
        <div className="px-5 py-3 rounded-full border ">2</div>
        <div className="px-5 py-3 rounded-full border ">3</div>
        <div className="px-5 py-3 rounded-full border ">4</div>
        <div className="px-5 py-3 rounded-full border ">5</div>

        <div className="p-3 rounded-full border">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className="w-6  h-6  "
          >
            <path
              fillRule="evenodd"
              d="M16.28 11.47a.75.75 0 0 1 0 1.06l-7.5 7.5a.75.75 0 0 1-1.06-1.06L14.69 12 7.72 5.03a.75.75 0 0 1 1.06-1.06l7.5 7.5Z"
              clipRule="evenodd"
            />
          </svg>
        </div>
      </div>
    </section>
  );
};

export default Pagnigation;
