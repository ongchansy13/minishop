import choose1 from "../assets/choose-1.jpg";
import choose2 from "../assets/choose-2.jpg";
import choose3 from "../assets/choose-3.jpg";

const Promotion = () => {
  return (
    <section className="">
      <div className="grid gap-5 md:gap-0 md:grid-cols-3 max-w-[1200px] mx-auto">
        <div
          className="md:row-span-2 bg-yellow-200 bg-cover object-cover h-[600px] md:h-auto relative"
          style={{ backgroundImage: `url(${choose1})` }}
        >
          <div className="grid place-items-center gap-5 absolute top-[50%] md:top-[57%] p-10 text-white">
            <p className="font-bold ">MEN&apos;S SHOES</p>
            <h2 className="font-bold text-2xl">Men&apos;s Collection</h2>
            <p className="text-center">
              Separated they live in Bookmarksgrove right at the coast of the
              Semantics, a large language ocean.
            </p>
            <p className="py-2 px-6 bg-slate-800 cursor-pointer transition-all hover:bg-yellow-800 rounded-full text-white">
              shop now
            </p>
          </div>
        </div>
        <div
          className="md:col-span-2 bg-yellow-100 bg-cover  p-20 relative h-[400px] "
          style={{ backgroundImage: `url(${choose2})` }}
        >
          <div className="grid absolute left-[50%] top-16">
            <div className="grid  text-white justify-self-end gap-2 md:gap-5 ">
              <p className="font-bold ">WOMEN&apos;S SHOES</p>
              <h2 className="font-bold text-2xl">WOMEN&apos;s COLLECTIONS</h2>
              <p className="">
                Separated they live in Bookmarksgrove right at the coast of the
                Semantics, a large language ocean.
              </p>
              <p className="w-[120px] py-2 px-6 cursor-pointer transition-all hover:bg-yellow-800 bg-slate-800 flex rounded-full text-white">
                shop now
              </p>
            </div>
          </div>
        </div>
        <div className="bg-slate-100 grid place-items-center">
          <div className="grid place-items-center p-10  justify-self-end  gap-5 ">
            <p className="font-bold text-center ">SUMMER SALE</p>
            <h2 className="font-extrabold text-3xl text-center">
              Extra 50% Off
            </h2>
            <p className="text-center">
              Separated they live in Bookmarksgrove right at the coast of the
              Semantics, a large language ocean.
            </p>
            <p className="w-[120px] py-2 px-6 cursor-pointer transition-all hover:bg-yellow-800 bg-slate-800 flex rounded-full text-white">
              shop now
            </p>
          </div>
        </div>
        <div
          className=" bg-cover object-cover relative  h-[300px]"
          style={{ backgroundImage: `url(${choose3})` }}
        >
          <div className="grid place-items-center p-10  text-white justify-self-end  gap-5 ">
            <p className="font-bold text-center ">SHOES</p>
            <h2 className="font-bold text-xl text-center">BEST SELLER</h2>
            <p className="text-center">
              Separated they live in Bookmarksgrove right at the coast of the
              Semantics, a large language ocean.
            </p>
            <p className="w-[120px] py-2 px-6 bg-slate-800 cursor-pointer transition-all hover:bg-yellow-800 flex rounded-full text-white">
              shop now
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Promotion;
