import prod1 from "../assets/prod-1.png";

import product2 from "../assets/product-2.png";
import product4 from "../assets/product-4.png";
import product6 from "../assets/product-6.png";
import Countdown from "./Countdown";

const MonthDiscount = () => {
  const targetDate = new Date("2025-01-31T00:00:00").getTime();

  return (
    <section className=" py-20 w-full bg-yellow-600">
      <div className="max-w-[1200px] mx-auto grid md:grid-cols-2 place-items-center justify-center gap-5 items-center">
        <div className="justify-self-center w-[50%] md:w-full md:justify-self-end">
          <img className="w-full md:w-full" src={prod1} alt="" />
        </div>

        <div className="">
          <div className="flex flex-col gap-5 text-white">
            <p className="text-lg font-bold">DEAL OF THE MONTH</p>
            <h1 className="text-5xl font-extrabold">Deal of the month</h1>

            <Countdown targetDate={targetDate} />

            <h3 className="text-black text-2xl font-bold">
              Nike Free RN 2019 iD
            </h3>
            <div className="flex items-center gap-2 ">
              <p className="font-bold text-xl text-black line-through">
                $120.00
              </p>
              <p className="font-bold text-xl">$80.00</p>
            </div>
          </div>

          <div className="flex items-center gap-5">
            <img className="w-24" src={product2} alt="" />
            <img className="w-24" src={product4} alt="" />
            <img className="w-24" src={product6} alt="" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default MonthDiscount;
