import { useState } from "react";
import { motion } from "framer-motion";

import person1 from "../assets/person_1.jpg";
import person2 from "../assets/person_2.jpg";
import person3 from "../assets/person_3.jpg";
import person4 from "../assets/person_4.jpg";

const customers = [
  {
    name: "john",
    img: person1,
    describe:
      "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    job: "NTERFACE DESIGNER",
  },
  {
    name: "jane",
    img: person2,
    describe:
      "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    job: "manager",
  },
  {
    name: "dave",
    img: person3,
    describe:
      "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    job: "manager",
  },
  {
    name: "dave",
    img: person4,
    describe:
      "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    job: "manager",
  },
];

const OurCustomers = () => {
  const [currentSlide, setCurrentSlide] = useState(0);

  const nextSlide = () => {
    setCurrentSlide((currentSlide) => (currentSlide + 1) % customers.length);
  };

  return (
    <section className="bg-slate-100 mb-5">
      <div className="max-w-[1200px] mx-auto grid place-items-center md:grid-cols-2 gap-8 py-20">
        <div className="md:justify-self-end">
          <motion.div
            initial="hidden"
            whileInView="visible"
            viewport={{ once: true, amount: 0.8 }}
            transition={{ duration: 0.5 }}
            variants={{
              hidden: { opacity: 0, y: 50 },
              visible: { opacity: 1, y: 0 },
            }}
            className="flex items-center gap-7 bg-slate-100 px-12 py-3"
          >
            <div>
              <ion-icon
                style={{ fontSize: "80px" }}
                name="gift-outline"
              ></ion-icon>
            </div>
            <div className="flex flex-col gap-2">
              <p className="font-bold text-xl">Free Shipping</p>
              <p>Separated they live in. A small river</p>
              <p>named Duden flows</p>
            </div>
          </motion.div>

          <motion.div
            initial="hidden"
            whileInView="visible"
            viewport={{ once: true, amount: 0.8 }}
            transition={{ duration: 0.5 }}
            variants={{
              hidden: { opacity: 0, y: 50 },
              visible: { opacity: 1, y: 0 },
            }}
            className="flex items-center gap-7 bg-slate-200 px-12 py-3"
          >
            <div>
              <ion-icon
                style={{ fontSize: "80px" }}
                name="snow-sharp"
              ></ion-icon>
            </div>
            <div className="flex flex-col gap-2">
              <p className="font-bold text-xl">Free Shipping</p>
              <p>Separated they live in. A small river</p>
              <p>named Duden flows</p>
            </div>
          </motion.div>

          <motion.div
            initial="hidden"
            whileInView="visible"
            viewport={{ once: true, amount: 0.8 }}
            transition={{ duration: 0.5 }}
            variants={{
              hidden: { opacity: 0, y: 50 },
              visible: { opacity: 1, y: 0 },
            }}
            className="flex items-center gap-7 bg-yellow-600 px-12 py-3"
          >
            <div>
              <ion-icon
                style={{ fontSize: "80px" }}
                name="card-sharp"
              ></ion-icon>
            </div>
            <div className="flex flex-col gap-2">
              <p className="font-bold text-xl">Free Shipping</p>
              <p>Separated they live in. A small river</p>
              <p>named Duden flows</p>
            </div>
          </motion.div>

          <motion.div
            initial="hidden"
            whileInView="visible"
            viewport={{ once: true, amount: 0.8 }}
            transition={{ duration: 0.5 }}
            variants={{
              hidden: { opacity: 0, y: 50 },
              visible: { opacity: 1, y: 0 },
            }}
            className="flex items-center gap-7 bg-black text-white px-12 py-3"
          >
            <div>
              <ion-icon
                style={{ fontSize: "80px" }}
                name="bulb-sharp"
              ></ion-icon>
            </div>
            <div className="flex flex-col gap-2">
              <p className="font-bold text-xl">Free Shipping</p>
              <p>Separated they live in. A small river</p>
              <p>named Duden flows</p>
            </div>
          </motion.div>
        </div>

        <div className="flex flex-col gap-5 pt-5 p-5 md:p-0">
          <motion.div
            initial="hidden"
            whileInView="visible"
            viewport={{ once: true, amount: 0.8 }}
            transition={{ duration: 0.5 }}
            variants={{
              hidden: { opacity: 0, y: 50 },
              visible: { opacity: 1, y: 0 },
            }}
            className="flex flex-col gap-10 "
          >
            <h1 className="text-4xl font-extrabold">
              Our satisfied customer says
            </h1>
            <p className="opacity-[0.6] text-lg">
              Far far away, behind the word mountains, far from the countries
              Vokalia and Consonantia, there live the blind texts. Separated
              they live in
            </p>
          </motion.div>
          <div className="shadow p-4 bg-slate-200">
            {customers.map((c, index) => (
              <motion.div
                initial="hidden"
                whileInView="visible"

                transition={{ duration: 0.5, delay: 0.5 }}
                variants={{
                  hidden: { opacity: 0, x: 50 },
                  visible: { opacity: 1, x: 0 },
                }}
                key={index}
                className={`${index === currentSlide ? "block" : "hidden"}`}
              >
                <div className="flex flex-col gap-4 mt-5 ">
                  <div className="relative">
                    <div className="absolute -bottom-1 left-[11%] z-30">
                      <ion-icon
                        style={{ fontSize: "25px", color: "#b45309" }}
                        name="duplicate-sharp"
                      ></ion-icon>
                    </div>
                    <img
                      className="w-[90px] rounded-full z-10"
                      src={c.img}
                      alt=""
                    />
                  </div>
                  <div className="relative px-5">
                    <div className="absolute w-[1px] h-full left-0 bg-yellow-500"></div>
                    <p className="opacity-[0.7] text-xl ">{c.describe}</p>
                  </div>
                </div>

                <div className="flex flex-col gap-1 mt-5">
                  <h5 className="font-bold text-xl text-yellow-600">
                    {c.name}
                  </h5>
                  <p className="font-thin text-sm opacity-[0.5] uppercase">
                    {c.job}
                  </p>
                </div>
              </motion.div>
            ))}
            <div className="flex items-center gap-2 mt-5">
              <div
                className="w-[10px] h-[10px] rounded-full bg-yellow-600"
                onClick={nextSlide}
              ></div>
              <div
                className="w-[10px] h-[10px] rounded-full bg-yellow-600"
                onClick={nextSlide}
              ></div>
              <div
                className="w-[10px] h-[10px] rounded-full bg-yellow-600"
                onClick={nextSlide}
              ></div>
              <div
                className="w-[10px] h-[10px] rounded-full bg-yellow-600"
                onClick={nextSlide}
              ></div>
              <div
                className="w-[10px] h-[10px] rounded-full bg-yellow-600"
                onClick={nextSlide}
              ></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default OurCustomers;
