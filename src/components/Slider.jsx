import { useState } from "react";

import person1 from "../assets/person_1.jpg";
import person2 from "../assets/person_2.jpg";
import person3 from "../assets/person_3.jpg";
import person4 from "../assets/person_4.jpg";

const Slider = () => {
  const [currentSlide, setCurrentSlide] = useState(0);

  const slider = [
    {
      name: "john",
      img: person1,
    },
    {
      name: "jane",
      img: person2,
    },
    {
      name: "jim",
      img: person3,
    },
    {
      name: "james",
      img: person4,
    },
  ];

  const nextSlide = () => {
    setCurrentSlide((currentSlide) => (currentSlide + 1) % slider.length);
  };

  return (
    <section className="max-w-[700px] mx-auto]">
      <div>
        {slider.map((s, index) => (
          <div
            key={index}
            className={`${index === currentSlide ? "block" : "hidden"}`}
          >
            <p>{s.name}</p>
            <img src={s.img} alt={s.name} />
          </div>
        ))}
      </div>

      <div className="flex gap-5 mt-10">
        <div className="w-2 h-2 bg-red-500 " onClick={nextSlide}></div>
        <div className="w-2 h-2 bg-red-500 " onClick={nextSlide}></div>
        <div className="w-2 h-2 bg-red-500 " onClick={nextSlide}></div>
      </div>
    </section>
  );
};

export default Slider;
