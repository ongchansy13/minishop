import { motion } from "framer-motion";

import projuct1 from "../assets/product-1.png";
import projuct2 from "../assets/product-2.png";
import projuct3 from "../assets/product-3.png";
import projuct4 from "../assets/product-4.png";
import projuct5 from "../assets/product-5.png";
import projuct6 from "../assets/product-6.png";
import projuct7 from "../assets/product-7.png";
import projuct8 from "../assets/product-8.png";

const carts = [
  {
    cart: {
      productImg: projuct1,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct2,
      add: "ADD TO CART +",
      buy: "BUY NOW",
      discount: "50% off",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct3,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct4,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct5,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct6,
      add: "ADD TO CART +",
      buy: "BUY NOW",
      discount: "50% off",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct7,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
  {
    cart: {
      productImg: projuct8,
      add: "ADD TO CART +",
      buy: "BUY NOW",
    },
    footer: {
      title: "LIFESTYLE",
      desc: "NIKE FREE RN 2019 ID",
      price: "$120.00",
    },
  },
];

const ProductsNewArrival = () => {
  return (
    <>
      <main className="bg-gray-100 py-20">
        <motion.div
          initial="hidden"
          whileInView="visible"
          viewport={{ once: true, amount: 0.8 }}
          transition={{ duration: 0.5 }}
          variants={{
            hidden: { opacity: 0, y: 50 },
            visible: { opacity: 1, y: 0 },
          }}
        >
          <div>
            <h2 className="text-5xl text-center mb-5">New Shoes Arrival</h2>
            <p className="text-center font-bold text-slate-400 mb-10">
              Far far away, behind the word mountains, far from the countries
              Vokalia and Consonantia
            </p>
          </div>
        </motion.div>

        <section className="max-w-[1200px] mx-auto px-5 grid md:grid-cols-4 items-center justify-center gap-5">
          {carts.slice(0, 8).map((c, index) => {
            return (
              <section key={index}>
                <motion.div
                  initial={{
                    opacity: 0,
                    y: 50,
                  }}
                  whileInView={{
                    opacity: 1,
                    y: 0,
                  }}
                  viewport={{ once: true }}
                  transition={{
                    duration: 0.5,
                    delay: 0.2,
                  }}
                >
                  <div className="shadow-sm bg-slate-300">
                    <div className="group relative cursor-pointer">
                      <div className=" ">
                        <div className="overflow-hidden relative  ">
                          <div className="group-hover:opacity-100 opacity-0 absolute top-[50%] left-[50%] -translate-x-[50%] -translate-y-[50%] h-[300px] w-[300px] md:w-[220px] md:h-[220px] group-hover:bg-pink-500 rounded-full"></div>
                          <img
                            className=" transition-all duration-500 group-hover:scale-75 md:group-hover:scale-125  w-full"
                            src={c.cart.productImg}
                            alt=""
                          />
                        </div>
                      </div>

                      <div className="opacity-0  group-hover:opacity-100 flex items-center justify-center  transition-all duration-5000 z-40 absolute md:top-[85%] md:left-[5%] md:-translate-x-0 md:-translate-y-0 -translate-x-[50%] -translate-y-[50%] top-[90%] left-[45%] ">
                        <div className="bg-black hover:bg-yellow-600 text-white p-2  transition-all duration-300">
                          <p>{c.cart.add}</p>
                        </div>

                        <div className="flex items-center justify-center gap-2 p-2 bg-slate-200 hover:bg-yellow-600 transition-all duration-300">
                          <p>{c.cart.buy}</p>
                          <ion-icon name="cart"></ion-icon>
                        </div>
                      </div>
                      {index === 1 && (
                        <p className="bg-yellow-500 uppercase text-md p-2 absolute -rotate-90 top-5 -left-5">
                          50% off
                        </p>
                      )}
                      {index === 5 && (
                        <p className="bg-yellow-500 uppercase text-md p-2 absolute -rotate-90 top-5 -left-5">
                          50% off
                        </p>
                      )}
                    </div>
                    <div className="bg-white p-2">
                      <div className="flex items-center justify-between mb-2">
                        <p>{c.footer.title}</p>
                        <div>
                          <ion-icon
                            style={{ color: "orange" }}
                            name="star-outline"
                          ></ion-icon>
                          <ion-icon
                            style={{ color: "orange" }}
                            name="star-outline"
                          ></ion-icon>
                          <ion-icon
                            style={{ color: "orange" }}
                            name="star-outline"
                          ></ion-icon>
                          <ion-icon
                            style={{ color: "orange" }}
                            name="star-outline"
                          ></ion-icon>
                          <ion-icon
                            style={{ color: "orange" }}
                            name="star-outline"
                          ></ion-icon>
                        </div>
                      </div>
                      <div className="mb-2">
                        <h5 className="font-bold">{c.footer.desc}</h5>
                      </div>
                      <div>
                        <p className="font-thin">{c.footer.price}</p>
                      </div>
                    </div>
                  </div>
                </motion.div>
              </section>
            );
          })}
        </section>
      </main>
    </>
  );
};

export default ProductsNewArrival;
