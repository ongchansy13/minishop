import { Modal } from "flowbite-react";
import { useState } from "react";
import { motion } from "framer-motion";

import galery1 from "../assets/gallery-1.jpg";
import galery2 from "../assets/gallery-2.jpg";
import galery3 from "../assets/gallery-3.jpg";
import galery4 from "../assets/gallery-4.jpg";
import galery5 from "../assets/gallery-5.jpg";
import galery6 from "../assets/gallery-6.jpg";

const FollowUs = () => {
  const [openModal, setOpenModal] = useState(false);

  const sliders = [
    {
      img: galery1,
    },
    {
      img: galery2,
    },
    {
      img: galery3,
    },
    {
      img: galery4,
    },
    {
      img: galery5,
    },
    {
      img: galery6,
    },
  ];

  const [currentSlide, setCurrentSlide] = useState(0);

  const nextSlide = () => {
    setCurrentSlide((currentSlide) => (currentSlide + 1) % sliders.length);
  };

  return (
    <>
      <section className="overflow-hidden">
        <div className="grid items-center justify-center">
          <motion.div
            initial="hidden"
            whileInView="visible"
            viewport={{ once: true, amount: 0.8 }}
            transition={{ duration: 0.5 }}
            variants={{
              hidden: { opacity: 0, y: 50 },
              visible: { opacity: 1, y: 0 },
            }}
            className="flex flex-col gap-2 items-center pb-12"
          >
            <h1 className="text-center font-bold text-5xl mb-10">
              Follow Us On Instagram
            </h1>
            <p className="text-center">
              Far far away, behind the word mountains, far from the countries
              Vokalia and Consonantia, there live the blind texts. Separated
              they live in
            </p>
          </motion.div>

          <div className="flex flex-wrap md:flex-nowrap gap-1">
            <motion.div
              initial="hidden"
              whileInView="visible"
              viewport={{ once: true, amount: 0.8 }}
              transition={{ duration: 0.5 }}
              variants={{
                hidden: { opacity: 0, y: 50 },
                visible: { opacity: 1, y: 0 },
              }}
              className="relative group cursor-pointer"
            >
              <img
                className="md:w-[400px] md:h-[400px] bg-opacity-20"
                src={galery1}
                alt=""
              />
              <div className="absolute top-[50%] left-[50%] -translate-x-[50%] -translate-y-[50%]  pt-2 pl-2 pr-2 pb-1 bg-yellow-400 rounded-xl opacity-0 !overflow-visible group-hover:opacity-100 group-hover:overflow-visible transition-all duration-200">
                <ion-icon
                  style={{ fontSize: "30px", color: "#b45309" }}
                  name="logo-instagram"
                  onClick={() => setOpenModal(true)}
                ></ion-icon>
              </div>
            </motion.div>

            <motion.div
              initial="hidden"
              whileInView="visible"
              viewport={{ once: true, amount: 0.8 }}
              transition={{ duration: 0.5, delay: 0.1 }}
              variants={{
                hidden: { opacity: 0, y: 50 },
                visible: { opacity: 1, y: 0 },
              }}
              className="relative group cursor-pointer"
            >
              <img
                className="md:w-[400px] md:h-[400px] bg-opacity-20"
                src={galery2}
                alt=""
              />
              <div className="absolute top-[50%] left-[50%] -translate-x-[50%] -translate-y-[50%]  pt-2 pl-2 pr-2 pb-1 bg-yellow-400 rounded-xl opacity-0 !overflow-visible group-hover:opacity-100 group-hover:overflow-visible transition-all duration-200">
                <ion-icon
                  style={{ fontSize: "30px", color: "#b45309" }}
                  name="logo-instagram"
                  onClick={() => setOpenModal(true)}
                ></ion-icon>
              </div>
            </motion.div>

            <motion.div
              initial="hidden"
              whileInView="visible"
              viewport={{ once: true, amount: 0.8 }}
              transition={{ duration: 0.5, delay: 0.2 }}
              variants={{
                hidden: { opacity: 0, y: 50 },
                visible: { opacity: 1, y: 0 },
              }}
              className="relative group cursor-pointer"
            >
              <img
                className="md:w-[400px] md:h-[400px] bg-opacity-20"
                src={galery3}
                alt=""
              />
              <div className="absolute top-[50%] left-[50%] -translate-x-[50%] -translate-y-[50%]  pt-2 pl-2 pr-2 pb-1 bg-yellow-400 rounded-xl opacity-0 !overflow-visible group-hover:opacity-100 group-hover:overflow-visible transition-all duration-200">
                <ion-icon
                  style={{ fontSize: "30px", color: "#b45309" }}
                  name="logo-instagram"
                  onClick={() => setOpenModal(true)}
                ></ion-icon>
              </div>
            </motion.div>

            <motion.div
              initial="hidden"
              whileInView="visible"
              viewport={{ once: true, amount: 0.8 }}
              transition={{ duration: 0.5, delay: 0.3 }}
              variants={{
                hidden: { opacity: 0, y: 50 },
                visible: { opacity: 1, y: 0 },
              }}
              className="relative group cursor-pointer"
            >
              <img
                className="md:w-[400px] md:h-[400px] bg-opacity-20"
                src={galery4}
                alt=""
              />
              <div className="absolute top-[50%] left-[50%] -translate-x-[50%] -translate-y-[50%]  pt-2 pl-2 pr-2 pb-1 bg-yellow-400 rounded-xl opacity-0 !overflow-visible group-hover:opacity-100 group-hover:overflow-visible transition-all duration-200">
                <ion-icon
                  style={{ fontSize: "30px", color: "#b45309" }}
                  name="logo-instagram"
                  onClick={() => setOpenModal(true)}
                ></ion-icon>
              </div>
            </motion.div>

            <motion.div
              initial="hidden"
              whileInView="visible"
              viewport={{ once: true, amount: 0.8 }}
              transition={{ duration: 0.5, delay: 0.4 }}
              variants={{
                hidden: { opacity: 0, y: 50 },
                visible: { opacity: 1, y: 0 },
              }}
              className="relative group cursor-pointer"
            >
              <img
                className="md:w-[400px] md:h-[400px] bg-opacity-20"
                src={galery5}
                alt=""
              />
              <div className="absolute top-[50%] left-[50%] -translate-x-[50%] -translate-y-[50%]  pt-2 pl-2 pr-2 pb-1 bg-yellow-400 rounded-xl opacity-0 !overflow-visible group-hover:opacity-100 group-hover:overflow-visible transition-all duration-200">
                <ion-icon
                  style={{ fontSize: "30px", color: "#b45309" }}
                  name="logo-instagram"
                  onClick={() => setOpenModal(true)}
                ></ion-icon>
              </div>
            </motion.div>

            <motion.div
              initial="hidden"
              whileInView="visible"
              viewport={{ once: true, amount: 0.8 }}
              transition={{ duration: 0.5, delay: 0.4 }}
              variants={{
                hidden: { opacity: 0, y: 50 },
                visible: { opacity: 1, y: 0 },
              }}
              className="relative group cursor-pointer"
            >
              <img
                className="md:w-[400px] md:h-[400px] bg-opacity-20"
                src={galery6}
                alt=""
              />
              <div className="absolute top-[50%] left-[50%] -translate-x-[50%] -translate-y-[50%]  pt-2 pl-2 pr-2 pb-1 bg-yellow-400 rounded-xl opacity-0 !overflow-visible group-hover:opacity-100 group-hover:overflow-visible transition-all duration-200">
                <ion-icon
                  style={{ fontSize: "30px", color: "#b45309" }}
                  name="logo-instagram"
                  onClick={() => setOpenModal(true)}
                ></ion-icon>
              </div>
            </motion.div>
          </div>
        </div>
      </section>

      <motion.div
        initial={{
          opacity: 0,
          scale: 0,
        }}
        animate={{
          opacity: 1,
          scale: 1,
        }}
        transition={{
          duration: 0.5,
          delay: 0.2,
        }}
      >
        <Modal
          show={openModal}
          onClose={() => setOpenModal(false)}
          className="p-0 relative"
        >
          <div
            className="absolute -top-7 right-0"
            onClick={() => setOpenModal(false)}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="currentColor"
              className="w-6 h-6"
            >
              <path
                fillRule="evenodd"
                d="M5.47 5.47a.75.75 0 0 1 1.06 0L12 10.94l5.47-5.47a.75.75 0 1 1 1.06 1.06L13.06 12l5.47 5.47a.75.75 0 1 1-1.06 1.06L12 13.06l-5.47 5.47a.75.75 0 0 1-1.06-1.06L10.94 12 5.47 6.53a.75.75 0 0 1 0-1.06Z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <Modal.Body className="p-0">
            <div className="">
              {sliders.map((s, index) => (
                <div
                  key={index}
                  className={`${index === currentSlide ? "block" : "hidden"}`}
                >
                  <img className="w-full h-full" src={s.img} alt="" />
                </div>
              ))}
            </div>
          </Modal.Body>

          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-12 h-12 bg-gray-300  rounded-full absolute top-1/2 left-0 p-5 md:-left-20 cursor-pointer"
            onClick={nextSlide}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M15.75 19.5 8.25 12l7.5-7.5"
            />
          </svg>

          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-12 h-12 bg-gray-300  rounded-full absolute top-1/2 right-0 p-5 md:-right-20 cursor-pointer"
            onClick={nextSlide}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="m8.25 4.5 7.5 7.5-7.5 7.5"
            />
          </svg>
        </Modal>
      </motion.div>
    </>
  );
};

export default FollowUs;
