import { useState } from "react";
import { motion } from "framer-motion";

import product1 from "../assets/product-1.png";

const SingleProduct = () => {
  let [number, setNumber] = useState(0);

  const decreaseNum = () => {
    if (number === 0) {
      setNumber(0);
    } else {
      setNumber((number = number - 1));
    }
  };
  const increaseNum = () => {
    setNumber((number = number + 1));
  };
  return (
    <>
      <section className="py-12">
        <div className="max-w-[1200px] mx-auto grid px-10 md:grid-cols-[2fr,3fr] gap-12">
          <motion.div
            initial="hidden"
            whileInView="visible"
            viewport={{ once: true, amount: 0.8 }}
            transition={{ duration: 0.5 }}
            variants={{
              hidden: { opacity: 0, y: 50 },
              visible: { opacity: 1, y: 0 },
            }}
            className="shadow-sm bg-gray-200"
          >
            <img className="w-full h-auto" src={product1} alt="" />
          </motion.div>

          <motion.div
            initial="hidden"
            whileInView="visible"
            viewport={{ once: true, amount: 0.8 }}
            transition={{ duration: 0.5, delay: 0.2 }}
            variants={{
              hidden: { opacity: 0, y: 50 },
              visible: { opacity: 1, y: 0 },
            }}
            className="flex flex-col gap-12"
          >
            <div className="flex flex-col gap-5">
              <h1 className="text-5xl">Nike Free RN 2019 iD</h1>
              <div className="flex items-center gap-2">
                <p className="text-orange-200">5.0</p>
                <div className="flex items-center gap-1">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-4 h-4 text-orange-200"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M11.48 3.499a.562.562 0 0 1 1.04 0l2.125 5.111a.563.563 0 0 0 .475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 0 0-.182.557l1.285 5.385a.562.562 0 0 1-.84.61l-4.725-2.885a.562.562 0 0 0-.586 0L6.982 20.54a.562.562 0 0 1-.84-.61l1.285-5.386a.562.562 0 0 0-.182-.557l-4.204-3.602a.562.562 0 0 1 .321-.988l5.518-.442a.563.563 0 0 0 .475-.345L11.48 3.5Z"
                    />
                  </svg>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-4 h-4 text-orange-200"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M11.48 3.499a.562.562 0 0 1 1.04 0l2.125 5.111a.563.563 0 0 0 .475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 0 0-.182.557l1.285 5.385a.562.562 0 0 1-.84.61l-4.725-2.885a.562.562 0 0 0-.586 0L6.982 20.54a.562.562 0 0 1-.84-.61l1.285-5.386a.562.562 0 0 0-.182-.557l-4.204-3.602a.562.562 0 0 1 .321-.988l5.518-.442a.563.563 0 0 0 .475-.345L11.48 3.5Z"
                    />
                  </svg>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-4 h-4 text-orange-200"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M11.48 3.499a.562.562 0 0 1 1.04 0l2.125 5.111a.563.563 0 0 0 .475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 0 0-.182.557l1.285 5.385a.562.562 0 0 1-.84.61l-4.725-2.885a.562.562 0 0 0-.586 0L6.982 20.54a.562.562 0 0 1-.84-.61l1.285-5.386a.562.562 0 0 0-.182-.557l-4.204-3.602a.562.562 0 0 1 .321-.988l5.518-.442a.563.563 0 0 0 .475-.345L11.48 3.5Z"
                    />
                  </svg>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-4 h-4 text-orange-200"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M11.48 3.499a.562.562 0 0 1 1.04 0l2.125 5.111a.563.563 0 0 0 .475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 0 0-.182.557l1.285 5.385a.562.562 0 0 1-.84.61l-4.725-2.885a.562.562 0 0 0-.586 0L6.982 20.54a.562.562 0 0 1-.84-.61l1.285-5.386a.562.562 0 0 0-.182-.557l-4.204-3.602a.562.562 0 0 1 .321-.988l5.518-.442a.563.563 0 0 0 .475-.345L11.48 3.5Z"
                    />
                  </svg>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-4 h-4 text-orange-200"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M11.48 3.499a.562.562 0 0 1 1.04 0l2.125 5.111a.563.563 0 0 0 .475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 0 0-.182.557l1.285 5.385a.562.562 0 0 1-.84.61l-4.725-2.885a.562.562 0 0 0-.586 0L6.982 20.54a.562.562 0 0 1-.84-.61l1.285-5.386a.562.562 0 0 0-.182-.557l-4.204-3.602a.562.562 0 0 1 .321-.988l5.518-.442a.563.563 0 0 0 .475-.345L11.48 3.5Z"
                    />
                  </svg>
                </div>
                <div>
                  <p className=" font-medium">
                    100{" "}
                    <span className=" text-gray-500 font-medium"> Rating</span>
                  </p>
                </div>
                <div>
                  <p className=" font-medium">
                    50 <span className=" text-gray-500 font-medium">Sold</span>{" "}
                  </p>
                </div>
              </div>
            </div>

            <div className="flex flex-col gap-7">
              <div>
                <h2 className="text-5xl">$120.00</h2>
              </div>

              <div className="flex flex-col gap-5">
                <p className="font-sm text-gray-600 opacity-55">
                  A small river named Duden flows by their place and supplies it
                  with the necessary regelialia. It is a paradisematic country,
                  in which roasted parts of sentences fly into your mouth.
                </p>
                <div>
                  <p className="font-sm text-gray-600 opacity-55">
                    On her way she met a copy. The copy warned the Little Blind
                    Text, that where it came from it would have been rewritten a
                    thousand times and everything that was left from its origin
                    would be the word &ldquo; and &ldquo; and the Little Blind
                    Text should turn around and return to its own, safe country.
                    But nothing the copy said could convince her and so it
                    didn’t take long until a few insidious Copy Writers ambushed
                    her, made her drunk with Longe and Parole and dragged her
                    into their agency, where they abused her for their.
                  </p>
                </div>
              </div>
            </div>

            <div className="flex flex-col gap-5">
              <div>
                <select className="px-5 py-2 outline outline-1">
                  <option>SMALL</option>
                  <option>MEDIUM</option>
                  <option>LARGE</option>
                  <option>EXTRA LARGE</option>
                </select>
              </div>

              <div className="flex items-center gap-2">
                <div
                  className="px-5 py-2 border border-1 cursor-pointer"
                  onClick={decreaseNum}
                >
                  -
                </div>
                <div className="px-20 py-2 border border-1">{number}</div>
                <div
                  className="px-5 py-2 border border-1 cursor-pointer"
                  onClick={increaseNum}
                >
                  +
                </div>
              </div>

              <p className="text-slate-700 text-sm font-bold">
                80 piece available
              </p>
            </div>

            <div className="flex items-center gap-5">
              <button>
                <a
                  href="#"
                  className="px-8 py-4 bg-black text-white font-bold text-sm rounded-full hover:bg-yellow-600 hover:text-white transition-all duration-200"
                >
                  Add to cart
                </a>
              </button>
              <button>
                <a
                  href="#"
                  className="px-8 py-4 bg-yellow-600 text-white font-bold text-sm rounded-full hover:bg-inherit hover:outline hover:outline-1 hover:text-yellow-600 transition-all duration-200"
                >
                  Buy now
                </a>
              </button>
            </div>
          </motion.div>
        </div>
      </section>
    </>
  );
};

export default SingleProduct;
