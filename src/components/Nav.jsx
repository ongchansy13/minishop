import { Link } from "react-router-dom";

const Nav = () => {
  return (
    <div>
      <div className="container mx-auto flex items-center justify-between ">
        <div>logo</div>
        <ul className="flex items-center gap-8">
          <li>
            <Link to="/productnew">product</Link>
          </li>
          <li>
            <Link to="/service">service</Link>
          </li>
          {/* <li>
            <Link to='/category'>Category</Link>
          </li> */}
        </ul>
      </div>
    </div>
  );
};

export default Nav;
