import { motion } from "framer-motion";
import { Link } from "react-router-dom";

import event1 from "../assets/event1.jpg";
import event2 from "../assets/event2.jpg";
import event3 from "../assets/event3.jpg";
import event4 from "../assets/event4.jpg";
import event5 from "../assets/event5.jpg";

const eventsList = [
  {
    img: event1,
    title: "APRIL 9, 2019 ADMIN",
    desc: "Even the all-powerful Pointing has no control about the blind texts",
    detail:
      "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    btn: "Read more",
  },
  {
    img: event2,
    title: "APRIL 9, 2019 ADMIN",
    desc: "Even the all-powerful Pointing has no control about the blind texts",
    detail:
      "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    btn: "Read more",
  },
  {
    img: event3,
    title: "APRIL 9, 2019 ADMIN",
    desc: "Even the all-powerful Pointing has no control about the blind texts",
    detail:
      "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    btn: "Read more",
  },
  {
    img: event4,
    title: "APRIL 9, 2019 ADMIN",
    desc: "Even the all-powerful Pointing has no control about the blind texts",
    detail:
      "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    btn: "Read more",
  },
  {
    img: event5,
    title: "APRIL 9, 2019 ADMIN",
    desc: "Even the all-powerful Pointing has no control about the blind texts",
    detail:
      "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    btn: "Read more",
  },
];

const subEvent = [
  {
    img: event1,
    desc: "Even the all-powerful Pointing has no control about the blind texts",
  },
  {
    img: event2,
    desc: "Even the all-powerful Pointing has no control about the blind texts",
  },
  {
    img: event3,
    desc: "Even the all-powerful Pointing has no control about the blind texts",
  },
];

const BlogEvent = () => {
  return (
    <>
      <section className="lg:py-24 py-12">
        <div className="max-w-[1200px] mx-auto grid lg:grid-cols-[1fr,2fr] gap-12 ">
          <div className="lg:justify-self-end lg:row-span-full row-start-2">
            <div className="flex flex-col px-5  gap-12">
              <motion.div
                initial="hidden"
                whileInView="visible"
                viewport={{ once: true, amount: 0.8 }}
                transition={{ duration: 0.5 }}
                variants={{
                  hidden: { opacity: 0, y: 50 },
                  visible: { opacity: 1, y: 0 },
                }}
                className="flex items-center w-full  bg-none border border-black px-5 py-2 hover:border-orange-800  "
              >
                <input
                  type="text"
                  className=" border-none outline-none  bg-none placeholder:text-sm  w-full "
                  placeholder="TYPE A KEYWORD AND HIT ENTER"
                />
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  className="w-4 h-4"
                >
                  <path
                    fillRule="evenodd"
                    d="M10.5 3.75a6.75 6.75 0 1 0 0 13.5 6.75 6.75 0 0 0 0-13.5ZM2.25 10.5a8.25 8.25 0 1 1 14.59 5.28l4.69 4.69a.75.75 0 1 1-1.06 1.06l-4.69-4.69A8.25 8.25 0 0 1 2.25 10.5Z"
                    clipRule="evenodd"
                  />
                </svg>
              </motion.div>

              <motion.div
                initial="hidden"
                whileInView="visible"
                viewport={{ once: true, amount: 0.8 }}
                transition={{ duration: 0.5, delay: 0.2 }}
                variants={{
                  hidden: { opacity: 0, y: 50 },
                  visible: { opacity: 1, y: 0 },
                }}
                className="flex flex-col flex-wrap gap-7"
              >
                <h1 className="font-medium">CATEGORIES</h1>
                <div className="flex flex-col gap-5">
                  <div className="flex items-center justify-between  border-b pb-2">
                    <p>Shoes</p>
                    <p className="opacity-25 font-medium">(12)</p>
                  </div>
                  <div className="flex items-center justify-between  border-b pb-2">
                    <p>Men&apos; Shoes</p>
                    <p className="opacity-25 font-medium">(22)</p>
                  </div>
                  <div className="flex items-center justify-between  border-b pb-2">
                    <p>Women&apos;s</p>
                    <p className="opacity-25 font-medium">(37)</p>
                  </div>
                  <div className="flex items-center justify-between  border-b pb-2">
                    <p>Accessories</p>
                    <p className="opacity-25 font-medium">(42)</p>
                  </div>
                  <div className="flex items-center justify-between  border-b pb-2">
                    <p>Sports</p>
                    <p className="opacity-25 font-medium">(14)</p>
                  </div>
                  <div className="flex items-center justify-between">
                    <p>Lifestyle</p>
                    <p className="opacity-25 font-medium">(140)</p>
                  </div>
                </div>
              </motion.div>

              <div className="flex flex-col items-center gap-5">
                {subEvent.map((subevent, index) => (
                  <div key={index}>
                    <motion.div
                      initial="hidden"
                      whileInView="visible"
                      viewport={{ once: true, amount: 0.8 }}
                      transition={{ duration: 0.5, delay: 0.3 }}
                      variants={{
                        hidden: { opacity: 0, y: 50 },
                        visible: { opacity: 1, y: 0 },
                      }}
                      className="grid md:grid-cols-[1fr,2fr]  gap-7"
                    >
                      <div className="">
                        <img
                          className="w-full full "
                          src={subevent.img}
                          alt=""
                        />
                      </div>
                      <div className="flex flex-col gap-7">
                        <p className=" text-gray-500 text-xl font-thin">
                          {subevent.desc}
                        </p>
                        <div className="flex items-center gap-1 text-sm opacity-50">
                          <div>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 24 24"
                              fill="currentColor"
                              className="w-3 h-3"
                            >
                              <path d="M12.75 12.75a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM7.5 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM8.25 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM9.75 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM10.5 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM12 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM12.75 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM14.25 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM15 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM16.5 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM15 12.75a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM16.5 13.5a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5Z" />
                              <path
                                fillRule="evenodd"
                                d="M6.75 2.25A.75.75 0 0 1 7.5 3v1.5h9V3A.75.75 0 0 1 18 3v1.5h.75a3 3 0 0 1 3 3v11.25a3 3 0 0 1-3 3H5.25a3 3 0 0 1-3-3V7.5a3 3 0 0 1 3-3H6V3a.75.75 0 0 1 .75-.75Zm13.5 9a1.5 1.5 0 0 0-1.5-1.5H5.25a1.5 1.5 0 0 0-1.5 1.5v7.5a1.5 1.5 0 0 0 1.5 1.5h13.5a1.5 1.5 0 0 0 1.5-1.5v-7.5Z"
                                clipRule="evenodd"
                              />
                            </svg>
                          </div>
                          <div>April 27,2019</div>
                          <div>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              viewBox="0 0 24 24"
                              strokeWidth={1.5}
                              stroke="currentColor"
                              className="w-3 h-3"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M15.75 6a3.75 3.75 0 1 1-7.5 0 3.75 3.75 0 0 1 7.5 0ZM4.501 20.118a7.5 7.5 0 0 1 14.998 0A17.933 17.933 0 0 1 12 21.75c-2.676 0-5.216-.584-7.499-1.632Z"
                              />
                            </svg>
                          </div>
                          <div>Admin</div>
                          <div>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 24 24"
                              fill="currentColor"
                              className="w-3 h-3"
                            >
                              <path
                                fillRule="evenodd"
                                d="M4.848 2.771A49.144 49.144 0 0 1 12 2.25c2.43 0 4.817.178 7.152.52 1.978.292 3.348 2.024 3.348 3.97v6.02c0 1.946-1.37 3.678-3.348 3.97a48.901 48.901 0 0 1-3.476.383.39.39 0 0 0-.297.17l-2.755 4.133a.75.75 0 0 1-1.248 0l-2.755-4.133a.39.39 0 0 0-.297-.17 48.9 48.9 0 0 1-3.476-.384c-1.978-.29-3.348-2.024-3.348-3.97V6.741c0-1.946 1.37-3.68 3.348-3.97ZM6.75 8.25a.75.75 0 0 1 .75-.75h9a.75.75 0 0 1 0 1.5h-9a.75.75 0 0 1-.75-.75Zm.75 2.25a.75.75 0 0 0 0 1.5H12a.75.75 0 0 0 0-1.5H7.5Z"
                                clipRule="evenodd"
                              />
                            </svg>
                          </div>
                          <div>19</div>
                        </div>
                      </div>
                    </motion.div>
                  </div>
                ))}
              </div>

              <motion.div
                initial="hidden"
                whileInView="visible"
                viewport={{ once: true, amount: 0.8 }}
                transition={{ duration: 0.5, delay: 0.4 }}
                variants={{
                  hidden: { opacity: 0, y: 50 },
                  visible: { opacity: 1, y: 0 },
                }}
                className="flex flex-col  gap-5"
              >
                <h1>TAG CLOUD</h1>
                <div className="flex items-center gap-2 flex-wrap">
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    Shoes
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    Products
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    shirt
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    jeans
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    shop
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    dress
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    coats
                  </div>
                  <div className="px-3 py-1 border uppercase text-sm opacity-75 ">
                    jusmpsuits
                  </div>
                </div>
              </motion.div>

              <motion.div
                initial="hidden"
                whileInView="visible"
                viewport={{ once: true, amount: 0.8 }}
                transition={{ duration: 0.5, delay: 0.5 }}
                variants={{
                  hidden: { opacity: 0, y: 50 },
                  visible: { opacity: 1, y: 0 },
                }}
                className="flex flex-col  gap-5"
              >
                <h1 className="font-meduim">PARAGRAPH</h1>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Ducimus itaque, autem necessitatibus voluptate quod mollitia
                  delectus aut, sunt placeat nam vero culpa sapiente consectetur
                  similique, inventore eos fugit cupiditate numquam!
                </p>
              </motion.div>
            </div>
          </div>

          <div className="lg:justify-self-start lg:row-span-full row-start-1">
            <div className="flex flex-col gap-12 px-5">
              {eventsList.map((event, index) => (
                <div key={index}>
                  <motion.div
                    initial="hidden"
                    whileInView="visible"
                    viewport={{ once: true, amount: 0.8 }}
                    transition={{ duration: 0.5 }}
                    variants={{
                      hidden: { opacity: 0, y: 50 },
                      visible: { opacity: 1, y: 0 },
                    }}
                    className=" grid lg:grid-cols-[2fr,3fr]  gap-5"
                  >
                    <div className="w-full">
                      <img className="w-full h-full" src={event.img} alt="" />
                    </div>
                    <div className="flex flex-col gap-5">
                      <p className="font-thin">{event.title}</p>
                      <p className="font-medium text-gray-500 text-sm md:text-xl">
                        {event.desc}
                      </p>
                      <p className="text-sm">{event.detail}</p>
                      <button className="flex border-none outline-none ">
                        <Link
                          to="/singleblog"
                          className="bg-yellow-600 px-4 py-2 text-sm font-medium text-gray-500  hover:bg-inherit hover:outline hover:outline-1 hover:border-yellow-600 hover:text-yellow-600 transition-all duration-200 rounded-full"
                        >
                          {event.btn}
                        </Link>
                      </button>
                    </div>
                  </motion.div>
                </div>
              ))}
              <div className="flex items-center gap-1">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  className="w-14  h-14 p-3  rounded-full border"
                >
                  <path
                    fillRule="evenodd"
                    d="M7.72 12.53a.75.75 0 0 1 0-1.06l7.5-7.5a.75.75 0 1 1 1.06 1.06L9.31 12l6.97 6.97a.75.75 0 1 1-1.06 1.06l-7.5-7.5Z"
                    clipRule="evenodd"
                  />
                </svg>

                <div className="px-5 py-3 rounded-full border bg-black text-white">
                  1
                </div>
                <div className="px-5 py-3 rounded-full border ">2</div>
                <div className="px-5 py-3 rounded-full border ">3</div>
                <div className="px-5 py-3 rounded-full border ">4</div>
                <div className="px-5 py-3 rounded-full border ">5</div>

                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  className="w-14  h-14 p-3  rounded-full border"
                >
                  <path
                    fillRule="evenodd"
                    d="M16.28 11.47a.75.75 0 0 1 0 1.06l-7.5 7.5a.75.75 0 0 1-1.06-1.06L14.69 12 7.72 5.03a.75.75 0 0 1 1.06-1.06l7.5 7.5Z"
                    clipRule="evenodd"
                  />
                </svg>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default BlogEvent;
