const Contactform = () => {
  return (
    <>
      <section className=" py-10  bg-slate-50 ">
        <div className="max-w-[1200px] mx-auto grid items-center gap-5  ">
          <div className=" grid lg:grid-cols-4 mx-5  md:gap-5">
            <div className="shadow bg-white pl-10 md:px-12 py-5">
              <p>Address: 198 West 21th</p>
              <p>Street, Suite 721 New York</p>
              <p>NY 10016</p>
            </div>
            <div className="shadow bg-white pl-10 md:px-12 py-5">
              <p>Phone: + 1235 2355 98</p>
            </div>
            <div className="shadow bg-white pl-10 md:px-12 py-5">
              <p>Email: info@yoursite.com</p>
            </div>
            <div className="shadow bg-white pl-10 md:px-12 py-5">
              <p>Website yoursite.com</p>
            </div>
          </div>

          <div className="grid  lg:grid-cols-2 gap-5 pt-5 mx-auto">
            <div className="justify-self-end sm:row-start-2 lg:row-span-full">
              <div className="flex flex-col items-center gap-5 justify-center p-32 shadow">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  className="w-16 h-16"
                >
                  <path
                    fillRule="evenodd"
                    d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12ZM12 8.25a.75.75 0 0 1 .75.75v3.75a.75.75 0 0 1-1.5 0V9a.75.75 0 0 1 .75-.75Zm0 8.25a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5Z"
                    clipRule="evenodd"
                  />
                </svg>

                <h4 className="text-2xl text-center">
                  Oops! Something went wrong.
                </h4>
                <p className="text-center text-sm">
                  This page didn&apos;t load Google Maps correctly. See the
                  JavaScript console for technical details.
                </p>
              </div>
            </div>
            <div className="bg-white p-6 row-start-1 lg:row-span-full ">
              <form action="" className="grid items-center gap-5">
                <div>
                  <input
                    className="w-full py-3 px-5 border"
                    type="text"
                    placeholder="Your Name"
                  />
                </div>
                <div>
                  <input
                    className="w-full py-3 px-5 border"
                    type="text"
                    placeholder="Your Email"
                  />
                </div>
                <div>
                  <input
                    className="w-full py-3 px-5 border"
                    type="text"
                    placeholder="Subject"
                  />
                </div>
                <div>
                  <textarea
                    className="w-full pt-2 border  placeholder:pl-5 placeholder:pt-5"
                    placeholder="Message"
                    rows={5}
                    cols={50}
                  ></textarea>
                </div>
              </form>
              <button type="submit" className="mt-4">
                <a href="#" className="px-10 py-4 bg-yellow-400 rounded-full">
                  Send Message
                </a>
              </button>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Contactform;
