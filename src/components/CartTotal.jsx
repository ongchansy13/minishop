import { motion } from "framer-motion";

const CartTotal = () => {
  return (
    <>
      <motion.section
        initial="hidden"
        whileInView="visible"
        viewport={{ once: true, amount: 0.8 }}
        transition={{ duration: 0.5 }}
        variants={{
          hidden: { opacity: 0, y: 50 },
          visible: { opacity: 1, y: 0 },
        }}
      >
        <div className=" p-20 shadow bg-slate-100 flex flex-col md:flex">
          <h1 className="mb-5">CART TOTAL</h1>

          <div className="">
            <div className="flex justify-between">
              <p className="mb-2">Subtotal</p>
              <p>$20.60</p>
            </div>

            <div className="flex justify-between">
              <p className="mb-2">Delivery</p>
              <p>$0.00</p>
            </div>

            <div className="flex justify-between  border-b">
              <p className="mb-2">Discount</p>
              <p>$3.00</p>
            </div>

            <div className="flex justify-between mt-5">
              <p className="mb-2">TOTAL</p>
              <p>$17.60</p>
            </div>
          </div>
        </div>
      </motion.section>
    </>
  );
};

export default CartTotal;
