import video from "../assets/video.mp4";
import about from "../assets/about.jpg";

import { Modal } from "flowbite-react";
import { useState } from "react";
import { motion } from "framer-motion";

const StablishOurCompany = () => {
  const [openModal, setOpenModal] = useState(false);
  return (
    <section className="bg-slate-50 py-5 px-5">
      <div className="max-w-[1200px] mx-auto grid items-center md:grid-cols-2 gap-5 p-5 ">
        <div className="relative">
          <div>
            <img src={about} alt="" />
            <div
              className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2"
              onClick={() => setOpenModal(true)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                className="w-24 h-24 bg-yellow-500 p-8 rounded-full text-white"
              >
                <path
                  fillRule="evenodd"
                  d="M4.5 5.653c0-1.427 1.529-2.33 2.779-1.643l11.54 6.347c1.295.712 1.295 2.573 0 3.286L7.28 19.99c-1.25.687-2.779-.217-2.779-1.643V5.653Z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
          </div>

          <Modal
            show={openModal}
            onClose={() => setOpenModal(false)}
            className="relative"
          >
            <div
              className="absolute -top-7 right-0"
              onClick={() => setOpenModal(false)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                className="w-6 h-6"
              >
                <path
                  fillRule="evenodd"
                  d="M5.47 5.47a.75.75 0 0 1 1.06 0L12 10.94l5.47-5.47a.75.75 0 1 1 1.06 1.06L13.06 12l5.47 5.47a.75.75 0 1 1-1.06 1.06L12 13.06l-5.47 5.47a.75.75 0 0 1-1.06-1.06L10.94 12 5.47 6.53a.75.75 0 0 1 0-1.06Z"
                  clipRule="evenodd"
                />
              </svg>
            </div>

            <Modal.Body className="p-0">
              <video width="700px" height="700px" controls>
                <source src={video} type="video/mp4" />
              </video>
            </Modal.Body>
          </Modal>
        </div>
        <motion.div
          initial="hidden"
          whileInView="visible"
          viewport={{ once: true, amount: 0.8 }}
          transition={{ duration: 0.5 }}
          variants={{
            hidden: { opacity: 0, y: 50 },
            visible: { opacity: 1, y: 0 },
          }}
          className="flex flex-col gap-5"
        >
          <div className="flex flex-col gap-5">
            <h2 className="text-2xl lg:text-5xl font-bold text-gray-500 mb-3">
              Stablished Sinced 1975
            </h2>
            <p className="font-sm text-gray-400">
              But nothing the copy said could convince her and so it didn’t take
              long until a few insidious Copy Writers ambushed her, made her
              drunk with Longe and Parole and dragged her into their agency,
              where they abused her for their.
            </p>
            <p className="font-sm text-gray-400">
              But nothing the copy said could convince her and so it didn’t take
              long until a few insidious Copy Writers ambushed her.
            </p>
          </div>
          <button className="flex">
            <a
              className="px-5 py-2 bg-yellow-400 rounded-full text-white font-bold"
              href="#"
            >
              shop now
            </a>
          </button>
        </motion.div>
      </div>
    </section>
  );
};

export default StablishOurCompany;
