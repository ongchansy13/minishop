import { motion } from "framer-motion";

const Payment = () => {
  return (
    <>
      <section>
        <motion.div
          initial="hidden"
          whileInView="visible"
          viewport={{ once: true, amount: 0.8 }}
          transition={{ duration: 0.5 }}
          variants={{
            hidden: { opacity: 0, y: 50 },
            visible: { opacity: 1, y: 0 },
          }}
          className="shadow px-10 py-12"
        >
          <h1 className="mb-5">PAYMENT METHOD</h1>

          <div className="flex flex-col gap-3">
            <div className="flex items-center gap-2">
              <input type="radio" name="name" />
              <p>Direct Bank Tranfer</p>
            </div>
            <div className="flex items-center gap-2">
              <input type="radio" name="name" />
              <p>Check Payment</p>
            </div>
            <div className="flex items-center gap-2">
              <input type="radio" name="name" />
              <p> Paypal</p>
            </div>
            <div className="flex items-center gap-2">
              <input type="checkbox" name="name" />
              <p> I have read and accept the terms and conditions</p>
            </div>

            <button className="bg-yellow-500 px-5 py-3 rounded-full mt-5">
              <a href="#">Place on order</a>
            </button>
          </div>
        </motion.div>
      </section>
    </>
  );
};

export default Payment;
