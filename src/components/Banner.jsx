import bgImg1 from "../assets/bg_1.png";
import bgImg2 from "../assets/bg_2.png";

import { useState, useEffect } from "react";
import { motion } from "framer-motion";

const Banner = () => {
  const slides = [
    {
      name: "#NEW ARRIVAL",
      title: "SHOES COLLECTION 2019",
      desc: "A small river name duden flow by their place and supply it with the necessary regiallia. it is paradisematic country.",
      btn: "Diccover Now",
      img: bgImg1,
    },
    {
      name: "#NEW ARRIVAL",
      title: "NEW SHOES WINTER COLLECTION",
      desc: "A small river name duden flow by their place and supply it with the necessary regiallia. it is paradisematic country.",
      btn: "Diccover Now",
      img: bgImg2,
    },
  ];

  const [currentSlide, setCurrentSlide] = useState(0);

  useEffect(() => {
    const intervalId = setInterval(() => {
      // Update the current slide index
      setCurrentSlide((prevSlide) => (prevSlide + 1) % slides.length);
    }, 3000); // Interval set to 2000 milliseconds (2 seconds)

    return () => clearInterval(intervalId); // Cleanup function to clear the interval on component unmount
  });

  return (
    <section className=" px-5 h-[700px] bg-yellow-600">

      {slides.map((slide, index) => {
        return (
          <motion.div
            key={index}
            className={`w-full h-full ${index === currentSlide ? "flex" : "hidden"
              }`}
          >
            <div className="w-full h-full py-12 grid md:grid-cols-2 gap-12 items-center justify-between">
              <div className="flex flex-col items-center justify-center gap-6">
                <p className="bg-black py-3 px-6 font-bold text-white">
                  {slide.name}
                </p>
                <h1 className="text-4xl text-center line-clamp-6 font-bold text-white">
                  {slide.title}
                </h1>
                <p className="text-xl font-bold text-white text-center line-height-24 my-5">
                  {slide.desc}
                </p>
                <button className="">
                  <a
                    className="text-2xl font-bold text-white bg-orange-500 py-3 px-6 rounded-sm shadow-md hover:bg-orange-600 transition-all duration-200"
                    href=""
                  >
                    {slide.btn}
                  </a>
                </button>
              </div>

              <div className="">
                <img className=" " src={slide.img} alt="" />
              </div>
            </div>
          </motion.div>
        );
      })}
    </section>
  );
};

export default Banner;
