import { motion } from "framer-motion";

const Billing = () => {
  return (
    <>
      <section className="py-12">
        <motion.div
          initial="hidden"
          whileInView="visible"
          viewport={{ once: true, amount: 0.8 }}
          transition={{ duration: 0.5 }}
          variants={{
            hidden: { opacity: 0, y: 50 },
            visible: { opacity: 1, y: 0 },
          }}
          className="max-w-[1000px] mx-auto px-5"
        >
          <h1 className="mb-5 text-2xl font-bold">BILLING DETAILS</h1>

          <div className="flex flex-col gap-5">
            <div className="flex flex-col md:flex-row gap-5 ">
              <div className="w-full">
                <label className="">First Name</label>
                <input
                  type="text"
                  className="w-full mt-2 p-4 outline outline-1"
                />
              </div>
              <div className="w-full">
                <label className="">Last Name</label>
                <input
                  type="text"
                  className="w-full mt-2 p-4 outline outline-1"
                />
              </div>
            </div>

            <div>
              <label>State / Country</label>
              <select className="w-full mt-2  p-4 outline outline-1">
                <option>France</option>
                <option>Italy</option>
                <option>Japan</option>
                <option>Philipine</option>
                <option>Hong Kong</option>
                <option>South Korea</option>
              </select>
            </div>

            <div className="flex flex-col md:flex-row  gap-5 ">
              <div className="w-full">
                <label>Street Address</label>
                <div className="flex flex-col md:flex-row gap-5 mt-2">
                  <input
                    type="text"
                    className="w-full p-4 outline outline-1"
                    placeholder="House number,street name"
                  />
                  <input
                    type="text"
                    className="w-full p-4 outline outline-1"
                    placeholder="Apartment,Suite,Unit,etc:(option)"
                  />
                </div>
              </div>
            </div>

            <div className="flex flex-col md:flex-row gap-5 ">
              <div className="w-full">
                <label>Town / City</label>
                <input
                  type="text"
                  className="w-full p-4 outline outline-1 mt-2"
                />
              </div>
              <div className="w-full">
                <label>Postcode / ZIP *</label>
                <input
                  type="text"
                  className="w-full p-4 outline outline-1 mt-2"
                />
              </div>
            </div>

            <div className="flex flex-col md:flex-row gap-5 ">
              <div className="w-full">
                <label>Phone</label>
                <input
                  type="text"
                  className="w-full p-4 outline outline-1 mt-2"
                />
              </div>
              <div className="w-full">
                <label>Email Address</label>
                <input
                  type="text"
                  className="w-full p-4 outline outline-1 mt-2"
                />
              </div>
            </div>
          </div>

          <div className="flex flex-col md:flex-row md:items-center gap-5 mt-5">
            <div className="flex items-center gap-2">
              <input type="radio" id="1" name="name" />
              <p>Create an Account?</p>
            </div>
            <div className="flex items-center gap-2">
              <input type="radio" id="2" name="name" />
              <p>Ship to different address</p>
            </div>
          </div>
        </motion.div>
      </section>
    </>
  );
};

export default Billing;
