const HeaderContact = () => {
  return (
    <section className="  ">
      <div className=" md:max-w-[1200px] mx-auto px-5 gap-2 md:gap-0 grid md:grid-cols-3 items-center justify-between font-thin">
        <div className="flex items-center  gap-2">
          <ion-icon name="call-outline"></ion-icon>
          <div>
            <p> + 1235 2355 98</p>
          </div>
        </div>

        <div className="flex items-center  gap-2">
          <ion-icon name="paper-plane-outline"></ion-icon>
          <p>YOUREMAIL@EMAIL.COM</p>
        </div>

        <div>
          <p>3-5 BUSINESS DAYS DELIVERY & FREE RETURNS</p>
        </div>
      </div>
    </section>
  );
};

export default HeaderContact;
