import "./App.css";


import { createBrowserRouter, Route, createRoutesFromElements, RouterProvider } from 'react-router-dom';



import Header from "./components/Header";
import Home from "./page/Home";
import About from "./page/About";
import Blog from "./page/Blog";
import Contact from "./page/Contact";
import Cart from "./page/Cart";
import Shop from "./page/Shop";
import SignleProduct from "./page/SignleProduct";
import Checkout from "./page/Checkout";
import SingleBlog from "./page/SingleBlog";
import Error from "./page/Error";


const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Header />}>
      <Route index element={<Home />}></Route>
      <Route path="about" element={<About />}></Route>
      <Route path="shop" element={<Shop />}></Route>
      <Route path="singleproduct" element={<SignleProduct />}></Route>
      <Route path="checkout" element={<Checkout />}></Route>
      <Route path="blog" element={<Blog />}></Route>
      <Route path="singleblog" element={<SingleBlog />}></Route>
      <Route path="contact" element={<Contact />}></Route>
      <Route path="cart" element={<Cart />}></Route>
      <Route path="*" element={<Error />}></Route>
    </Route>
  )
);

function App() {
  return (
    <RouterProvider router={router} />
  );
}

export default App;
