import Footer from "../components/Footer";
import SingleProduct from "../components/SingleProduct";
import TabComponent from "../components/TabComponent";
import TopBanner from "../components/TopBanner";

const SignleProduct = () => {
  return (
    <>
      <TopBanner name="HOME" subname="SHOP" about="SINGLE PRODUCT" />
      <SingleProduct />
      <TabComponent />
      <Footer />
    </>
  );
};

export default SignleProduct;
