import Footer from "../components/Footer";
import TopBanner from "../components/TopBanner";

const Error = () => {
  return (
    <>
      <TopBanner name="HOME" subname="ERROR PAGE" about="Page NOT FOUND" />
      <Footer />
    </>
  );
};

export default Error;
