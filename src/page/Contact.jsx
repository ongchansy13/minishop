import FollowUs from "../components/FollowUs";
import TopBanner from "../components/TopBanner";
import Footer from "../components/Footer";
import Contactform from "../components/Contactform";
const Contact = () => {
  return (
    <>
      <TopBanner name="HOME" subname="CONTACT" about="CONTACT US" />
      <Contactform />
      <FollowUs />
      <Footer />
    </>
  );
};

export default Contact;
