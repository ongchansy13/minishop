import TopBanner from "../components/TopBanner"
import Service from "../components/Service"
import FollowUs from "../components/FollowUs"
import Footer from "../components/Footer"
import OurCustomers from "../components/OurCustomers"
import StablishOurCompany from "../components/StablishOurCompany"
const About = () => {
  return (
    <section className="">

      <TopBanner name='HOME' subname='ABOUT' about='ABOUT US' />
      <Service />
      <StablishOurCompany />
      <OurCustomers />
      <FollowUs />
      <Footer />

    </section>
  )
}

export default About