import Billing from "../components/Billing";
import Footer from "../components/Footer";
import PaymentMethods from "../components/PaymentMethods";
import TopBanner from "../components/TopBanner";

const Checkout = () => {
  return (
    <>
      <TopBanner name="HOME" subname="CHECKOUT" about="CHECKOUT" />
      <Billing />
      <PaymentMethods />
      <Footer />
    </>
  );
};

export default Checkout;
