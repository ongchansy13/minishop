import BlogSingleComponent from "../components/BlogSingleComponent";
import FollowUs from "../components/FollowUs";
import Footer from "../components/Footer";
import TopBanner from "../components/TopBanner";

const SingleBlog = () => {
  return (
    <>
      <section>
        <TopBanner name="HOME" subname="BLOG" about="SINGLE BLOG" />
        <BlogSingleComponent />
        <FollowUs />
        <Footer />
      </section>
    </>
  );
};

export default SingleBlog;
