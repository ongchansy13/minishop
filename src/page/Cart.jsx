import Footer from "../components/Footer";
import ProductsTable from "../components/ProductsTable";
import TopBanner from "../components/TopBanner";
import { motion } from "framer-motion";

const Cart = () => {
  return (
    <>
      <TopBanner name="HOME" subname="CART" about="MY WISHLIST" />
      <motion.div
        initial="hidden"
        whileInView="visible"
        viewport={{ once: true, amount: 0.2 }}
        transition={{ duration: 0.5 }}
        variants={{
          hidden: { opacity: 0, y: 50 },
          visible: { opacity: 1, y: 0 },
        }}
      >
        <ProductsTable />
      </motion.div>
      <Footer />
    </>
  );
};

export default Cart;
