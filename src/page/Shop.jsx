import FollowUs from "../components/FollowUs";
import Footer from "../components/Footer";
import ShopComponent from "../components/ShopComponent";
import TopBanner from "../components/TopBanner";

const Shop = () => {
  return (
    <>
      <TopBanner name="HOME" subname="SHOP" about="SHOP" />
      <ShopComponent />
      <FollowUs />
      <Footer />
    </>
  );
};

export default Shop;
