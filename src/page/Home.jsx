import Banner from "../components/Banner";
import ProductsNewArrival from "../components/ProductsNewArrival";
import Service from "../components/Service";
import Promotion from "../components/Promotion";
import MonthDiscount from "../components/MonthDiscount";
import OurCustomers from "../components/OurCustomers";
import FollowUs from "../components/FollowUs";
import Footer from "../components/Footer";

const Home = () => {
  return (
    <>
      <Banner />
      <Service />
      <ProductsNewArrival />
      <Promotion />
      <MonthDiscount />
      <OurCustomers />
      <FollowUs />
      <Footer />
    </>
  );
};

export default Home;
