import FollowUs from "../components/FollowUs";
import TopBanner from "../components/TopBanner";
import Footer from "../components/Footer";
import BlogEvent from "../components/BlogEvent";

const Blog = () => {
  return (
    <>
      <TopBanner name="HOME" subname="BLOG" about="BLOG" />
      <BlogEvent />
      <FollowUs />
      <Footer />
    </>
  );
};

export default Blog;
